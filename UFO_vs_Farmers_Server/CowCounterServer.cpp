#include "ServerSharedHeaders.h"

uint32_t CowCounterServer::Write(OutputMemoryBitStream & inOutputStream, uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	if (inDirtyState & ECNT_Count)
	{
		inOutputStream.Write((bool)true);
		inOutputStream.Write(m_cowCount, 5);

		
		writtenState |= ECNT_Count;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	return writtenState;
}

void CowCounterServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject(this);
}

void CowCounterServer::abductCowServer()
{
}


CowCounterServer::CowCounterServer()
{
}
