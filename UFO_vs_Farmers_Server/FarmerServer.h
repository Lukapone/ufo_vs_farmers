class FarmerServer : public FarmerObject
{
public:
	static GameObjectPtr StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new FarmerServer()); }
	
	virtual void Update();
	virtual void HandleDying() override;
	virtual ClassIdentification class_identification() override;
protected:
	FarmerServer();

private:

	void ServerProcessInput(float elapsed, const InputState & inInputState);

};