#include "ServerSharedHeaders.h"

UFO_beamServer::UFO_beamServer()
{
}

void UFO_beamServer::Update()
{
}

uint32_t UFO_beamServer::Write(OutputMemoryBitStream & inOutputStream, uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	if (inDirtyState & EBMS_Position)
	{
		inOutputStream.Write((bool)true);
		uint32_t posX = sprite.getPosition().x;
		uint32_t posY = sprite.getPosition().y;
		inOutputStream.Write(posX, 12);
		inOutputStream.Write(posY, 12);

		writtenState |= EBMS_Position;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	return writtenState;
}

//if the beam intersect cow it will take periodical damage from cow and then destroy the cow made by Lukas
void UFO_beamServer::checkAndApplyCollision(float elapsed, UFOServer * serverUFOPtr)
{
	//std::cout << "checking beam collision" << std::endl;
	for (auto & gameObj : ObjectManagerV2::sInstance->getUpdateList())
	{
		if (gameObj->class_identification() == ClassIdentification::CowServerObject)
		{
			if (gameObj->collisionRect.getGlobalBounds().intersects(this->collisionRect.getGlobalBounds()))
			{
				m_timeSinceStartBeam += elapsed;
				if (m_timeSinceStartBeam > 0.2)//this is in milliseconds
				{
					//std::cout << "damage " << std::endl;
					CowServer & objCow = dynamic_cast<CowServer&>(*gameObj);
					objCow.takeDamege(1);
					m_timeSinceStartBeam = 0;//reset the timer

					if (objCow.isDeath())
					{
						gameObj->SetDoesWantToDie(true);
						static_cast< Server* > (Engine::sInstance.get())->mCowCounter->abductCow();
						serverUFOPtr->addCowToScore();
						NetworkManagerServer::sInstance->SetStateDirty(static_cast< Server* > (Engine::sInstance.get())->mCowCounter->GetNetworkId(), static_cast< Server* > (Engine::sInstance.get())->mCowCounter->ECNT_Count);
						return;
					}
				}
			}
		}
	}
}

void UFO_beamServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject(this);
}
