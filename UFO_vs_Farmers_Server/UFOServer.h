class UFO_beamServer;

class UFOServer : public UFO
{
public:
	static GameObjectPtr StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new UFOServer()); }
	virtual void HandleDying() override;
	virtual ClassIdentification class_identification() override;
	virtual void Update();
	void addCowToScore();
	void takeDamageServer(int32_t amount);

protected:
	UFOServer();

private:

	void handleBeam();
	void ServerProcessInput(float elapsed, const InputState & inInputState);
	void AddAndUpdateBeam();
	void UpdateBeam();
	int m_CowScore{ 0 };
	float		mTimeOfNextShot;
	float		mTimeBetweenShots;
	GameObjectPtr mBeam;
};