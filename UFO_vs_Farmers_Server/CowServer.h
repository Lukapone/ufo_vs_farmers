class CowServer : public CowObject
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new CowServer()); }

	virtual uint32_t	Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;
	virtual ClassIdentification class_identification() override;
	virtual void HandleDying() override;
protected:
	CowServer();

};


