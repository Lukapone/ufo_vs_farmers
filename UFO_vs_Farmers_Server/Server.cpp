#include "ServerSharedHeaders.h"

bool Server::StaticInit()
{
	sInstance.reset( new Server() );

	return true;
}

Server::Server()
{

	GameObjectRegistry::sInstance->RegisterCreationFunction( 'CUFO', UFOServer::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction('CCOW', CowServer::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('BEAM', UFO_beamServer::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('FRMR', FarmerServer::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('CCNT', CowCounterServer::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('CGUN', GunServer::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('BULL', BulletServer::StaticCreate);
	InitNetworkManager();
	TextureHolder::StaticInit();
	TextureHolder::sInstance->loadTextures();
	FontHolder::StaticInit();
	FontHolder::sInstance->loadFonts();
	loadHighestScore();
}


int Server::Run()
{
	SetupWorld();

	return Engine::Run();
}

bool Server::InitNetworkManager()
{
	std::cout << "initializing network manager with port 42000" << std::endl;
	std::string portString = "42000";

	uint16_t port = stoi( portString );

	return NetworkManagerServer::StaticInit( port );
}

void Server::SetupWorld()
{
	//add cows into game
	GameObjectPtr toAddCow;
	//up to 31cows as it is packed
	int cowCount = 17;//!!!!!!!!!!!!!!!!!!!!!
	for (int i = 0; i < cowCount; i++)
	{
		
		toAddCow = GameObjectRegistry::sInstance->CreateGameObjectandAddToObjManager('CCOW');
		toAddCow->sprite.setPosition(getRandomMt19937(100,4000), 510);
		//NetworkManagerServer::sInstance->SetStateDirty(toAddCow->GetNetworkId(), std::static_pointer_cast<CowServer>(toAddCow)->ECowReplicationState::ECOW_Position);
	}
	
	GameObjectPtr cowCounter;
	cowCounter = GameObjectRegistry::sInstance->CreateGameObjectandAddToObjManager('CCNT');
	mCowCounter = std::static_pointer_cast<CowCounter>(cowCounter);
	mCowCounter->setCount(cowCount);
	mCowCounter->text.setString("Cow counter: " + std::to_string(cowCount));
}

void Server::DoFrame()
{
	NetworkManagerServer::sInstance->ProcessIncomingPackets();

	NetworkManagerServer::sInstance->CheckForDisconnects();

	Engine::DoFrame();

	render();

	NetworkManagerServer::sInstance->SendOutgoingPackets();

	//std::cout << m_nameOfPlayerHighScore << " " << m_highestPlayerScore << std::endl;
}

void Server::HandleNewClientWithUFO( ClientProxyPtr inClientProxy , std::string & name )
{
	
	int playerId = inClientProxy->GetPlayerId();
	SpawnUFOForPlayer(playerId, name);
}




void Server::SpawnUFOForPlayer(int inPlayerId, std::string & name)
{
	std::cout << "Spawned UFO on server that represent players UFO" << std::endl;
	UFOPtr ufo = std::static_pointer_cast< UFO >(GameObjectRegistry::sInstance->CreateGameObjectandAddToObjManager('CUFO'));
	ufo->SetPlayerId(inPlayerId);
	ufo->m_PlayerName = name;
}


void Server::HandleNewClientWithFarmer(ClientProxyPtr inClientProxy)
{
	int playerId = inClientProxy->GetPlayerId();
	SpawnFarmerForClient(playerId);
}

void Server::SpawnFarmerForClient(int inPlayerId)
{
	std::cout << "Spawned FARMER on server that represent players FARMER" << std::endl;
	FarmerPtr frmr = std::static_pointer_cast< FarmerObject >(GameObjectRegistry::sInstance->CreateGameObjectandAddToObjManager('FRMR'));
	frmr->m_farmersGun = std::static_pointer_cast< GunObject >(GameObjectRegistry::sInstance->CreateGameObjectandAddToObjManager('CGUN'));
	frmr->m_farmersGun->sprite.setPosition(frmr->sprite.getPosition().x + 27, frmr->sprite.getPosition().y + 65);	
	frmr->m_farmersGun->m_Bullet = std::static_pointer_cast< BulletObject >(GameObjectRegistry::sInstance->CreateGameObjectandAddToObjManager('BULL'));
	frmr->SetPlayerId(inPlayerId);	
}

void Server::processEvents()
{
	sf::Event event;
	while (mWindow.pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::Closed:
			mWindow.close();
			break;
		}

	}
}

void Server::render()
{
	mWindow.clear();
	int objectsDrawn = ObjectManagerV2::sInstance->Draw(mWindow);
	//std::cout << "objects drawn on server" << objectsDrawn << std::endl;
	ObjectManagerV2::sInstance->DrawDebug(mWindow);
	mWindow.display();
}

void Server::loadHighestScore()
{	
	std::ifstream inFile("highscore.txt");
	if (inFile)
	{
		inFile >> m_nameOfPlayerHighScore >> m_highestPlayerScore;
	}
	else
	{
		std::cout << "loading failed" << std::endl;
	}
}

void Server::updateHighScore(int score, std::string & name)
{
	if (score > m_highestPlayerScore)
	{
		m_highestPlayerScore = score;
		m_nameOfPlayerHighScore = name;
		std::ofstream oFile("highscore.txt");
		if (oFile)
		{
			oFile << m_nameOfPlayerHighScore << " " << m_highestPlayerScore << std::endl;
		}
		else
		{
			std::cout << "saving failed" << std::endl;
		}

	}
}



void Server::HandleLostClient( ClientProxyPtr inClientProxy )
{
	//kill client's cat
	//remove client from scoreboard
	int playerId = inClientProxy->GetPlayerId();
	KillDisconectedUFOorFarmer(playerId);
}

void Server::KillDisconectedUFOorFarmer(int inPlayerId)
{
	for (auto & gameObj : ObjectManagerV2::sInstance->getUpdateList())
	{
		if (gameObj->class_identification() == ClassIdentification::UFOServer)
		{
			if (std::static_pointer_cast<UFO>(gameObj)->GetPlayerId() == inPlayerId)
			{
				std::cout << "Found disconected ufo" << std::endl;
				std::static_pointer_cast<UFO>(gameObj)->SetDoesWantToDie(true);
				break;
			}
		}
		if (gameObj->class_identification() == ClassIdentification::FarmerServer)
		{
			if (std::static_pointer_cast<FarmerObject>(gameObj)->GetPlayerId() == inPlayerId)
			{
				std::cout << "Found disconected farmer" << std::endl;
				std::static_pointer_cast<FarmerObject>(gameObj)->SetDoesWantToDie(true);
				std::static_pointer_cast<FarmerObject>(gameObj)->m_farmersGun->SetDoesWantToDie(true);
				break;
			}
		}
	}
}
