class CowCounterServer : public CowCounter
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new CowCounterServer()); }

	virtual uint32_t	Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;
	virtual void HandleDying() override;
	void abductCowServer();
protected:
	CowCounterServer();

};