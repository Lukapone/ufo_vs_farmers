#include "ServerSharedHeaders.h"

void UFOServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject(this);
}

ClassIdentification UFOServer::class_identification()
{
	return ClassIdentification::UFOServer;
}

void UFOServer::Update()
{

	float deltaTime{ 0 };
	sf::Vector2f oldPosition = sprite.getPosition();;
	float oldRotation = sprite.getRotation();

	//are you controlled by a player?
	//if so, is there a move we haven't processed yet?
		ClientProxyPtr client = NetworkManagerServer::sInstance->GetClientProxy(GetPlayerId());
		if (client)
		{
			MoveList& moveList = client->GetUnprocessedMoveList();
			for (const Move& unprocessedMove : moveList)
			{
				const InputState& currentState = unprocessedMove.GetInputState();
				deltaTime = unprocessedMove.GetDeltaTime();
				ServerProcessInput(deltaTime, currentState);
			}

			moveList.Clear();
		}
	
		UFO::Update();
		this->checkAndApplyCollision(oldPosition);
		auto objBeam = std::static_pointer_cast<UFO_beamServer>(mBeam);
		if (objBeam != nullptr)
		{
			objBeam->checkAndApplyCollision(deltaTime, this);
		}

		if (oldPosition == sprite.getPosition())
		{
			//std::cout << "same position" << std::endl;
		}
		else
		{
			NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), ECRS_PositionChange);
			
		}

		if (oldRotation == sprite.getRotation())
		{

		}
		else
		{
			NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), ECRS_Rotation);
		}
		
}

void UFOServer::addCowToScore()
{
	m_CowScore++;
	static_cast<Server*> (Engine::sInstance.get())->updateHighScore(m_CowScore, m_PlayerName);
}

void UFOServer::takeDamageServer(int32_t amount)
{
	m_health -= amount;
	NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), ECRS_Health);
}

UFOServer::UFOServer()
{

}

void UFOServer::handleBeam()
{
}

void UFOServer::ServerProcessInput(float elapsed, const InputState & inInputState)
{
	if (inInputState.is_W_pressed() && !inInputState.is_S_pressed())
	{

		if (!m_bAcceleratingUP)
		{
			m_bAcceleratingUP = true;
			m_objState.acceleration.y = -m_accelerationUPDown;
			m_velocityLock_y = false;
		}

		if (-m_objState.velocity.y < m_maxVelocity)
		{
			accelerate_y(m_objState, elapsed);
		}
	}
	else
	{
		m_bAcceleratingUP = false;
	}

	if (inInputState.is_S_pressed() && !inInputState.is_W_pressed())
	{
		if (!m_bAcceleratingDown)
		{
			m_bAcceleratingDown = true;
			m_objState.acceleration.y = m_accelerationUPDown;
			m_velocityLock_y = false;
		}

		if (m_objState.velocity.y < m_maxVelocity)
		{
			accelerate_y(m_objState, elapsed);
		}

	}
	else
	{
		m_bAcceleratingDown = false;
	}
	
		if (inInputState.is_A_pressed() && !inInputState.is_D_pressed())
		{

			if (!m_bAcceleratingLeft)
			{
				m_bAcceleratingLeft = true;
				m_objState.acceleration.x = -m_accelerationSides;
				m_bRotatingLeft = true;
				m_velocityLock_x = false;
			}

			if (-m_objState.velocity.x < m_maxVelocity)
			{
				accelerate_x(m_objState, elapsed);

			}

			if (m_rotatedRightAngle > 0)
			{
				m_rotatedLeftAngle -= m_rotatedRightAngle;
				m_rotatedRightAngle = 0;
			}
			if (m_rotatedLeftAngle < 45)
			{
				m_rotatedLeftAngle += 0.5;
				collisionRect.rotate(-0.5);
			}

		}
		else
		{
			m_bRotatingLeft = false;
			m_bAcceleratingLeft = false;

		}
	
	if (inInputState.is_D_pressed() && !inInputState.is_A_pressed())
	{

		if (!m_bAcceleratingRight)
		{
			m_bAcceleratingRight = true;
			m_objState.acceleration.x = m_accelerationSides;
			m_velocityLock_x = false;
			m_bRotatingRight = true;
		}

		if (m_objState.velocity.x < m_maxVelocity)
		{
			accelerate_x(m_objState, elapsed);
		}

		if (m_rotatedLeftAngle > 0)
		{
			m_rotatedRightAngle -= m_rotatedLeftAngle;
			m_rotatedLeftAngle = 0;
		}
		if (m_rotatedRightAngle < 45)
		{
			m_rotatedRightAngle += 0.5;
			collisionRect.rotate(0.5);
		}
	}
	else
	{

		m_bRotatingRight = false;
		m_bAcceleratingRight = false;



	}

	//if you are not pressing left or right you will need to check the velocity if is positive or negative and acceleration and if they match proceed
	if (!m_bAcceleratingLeft && !m_bAcceleratingRight)
	{
		if (!m_velocityLock_x)
		{
			m_velocityLock_x = true;
			m_is_positive_x = m_objState.velocity.x > 0;

		}
		if (m_is_positive_x && m_objState.velocity.x > 0)
		{
			if (m_objState.acceleration.x > 0)
			{
				deaccelerate_x(m_objState, elapsed);
			}
			else
			{
				accelerate_x(m_objState, elapsed);
			}
		}
		else if (!m_is_positive_x && -m_objState.velocity.x > 0)
		{
			if (-m_objState.acceleration.x > 0)
			{
				deaccelerate_x(m_objState, elapsed);
			}
			else
			{
				accelerate_x(m_objState, elapsed);
			}
		}
		else
		{
			m_objState.velocity.x = 0;
		}
	}

	if (!m_bAcceleratingDown && !m_bAcceleratingUP)
	{
		if (!m_velocityLock_y)
		{
			m_velocityLock_y = true;
			m_is_positive_y = m_objState.velocity.y > 0;

		}

		if (m_is_positive_y && m_objState.velocity.y > 0)
		{
			if (m_objState.acceleration.y > 0)
			{
				deaccelerate_y(m_objState, elapsed);
			}
			else
			{
				accelerate_y(m_objState, elapsed);
			}

		}
		else if (!m_is_positive_y && -m_objState.velocity.y > 0)
		{
			if (-m_objState.acceleration.y > 0)
			{
				deaccelerate_y(m_objState, elapsed);
			}
			else
			{
				accelerate_y(m_objState, elapsed);
			}
		}
		else
		{
			m_objState.velocity.y = 0;
		}
	}

	////////////////////////////////////////// APPLY MOVEMENT //////////////////////////////////////////////////////
	move(m_objState, elapsed);



	if (!m_bRotatingLeft && !m_bRotatingRight)
	{
		if (m_rotatedLeftAngle > 0)
		{
			m_rotatedLeftAngle -= 0.5;
			collisionRect.rotate(0.5);
		}
		else if (m_rotatedRightAngle > 0)
		{
			m_rotatedRightAngle -= 0.5;
			collisionRect.rotate(-0.5);
		}
		else
		{
			collisionRect.setRotation(0);
		}


	}

	if (inInputState.is_C_pressed())
	{

		if (m_objState.velocity.x == 0 && m_objState.velocity.y == 0)
		{
			m_bBeaming = true;
		}
		else
		{
			m_bBeaming = false;
			if (m_bBeamActive)
			{
				mBeam->SetDoesWantToDie(true);			
				m_bBeamActive = false;
			}
		}
	}
	else
	{
		m_bBeaming = false;
		if (m_bBeamActive)
		{
			mBeam->SetDoesWantToDie(true);
			m_bBeamActive = false;
		}

	}

	m_previousBeamKeyState = inInputState.is_C_pressed();

	if (m_bBeaming)
	{
		AddAndUpdateBeam();
	}
}

void UFOServer::AddAndUpdateBeam()
{
	if (!m_bBeamActive)
	{
		//add it and make it active
		mBeam = std::static_pointer_cast<UFO_beamServer>(GameObjectRegistry::sInstance->CreateGameObjectandAddToObjManager('BEAM'));
		m_bBeamActive = true;
	}
	UpdateBeam();
}

void UFOServer::UpdateBeam()
{
	mBeam->sprite.setPosition(sprite.getPosition().x, sprite.getPosition().y + 54);
	mBeam->collisionRect.setPosition(sprite.getPosition().x, sprite.getPosition().y + 54);
	NetworkManagerServer::sInstance->SetStateDirty(mBeam->GetNetworkId(), ECRS_PositionChange);
}













