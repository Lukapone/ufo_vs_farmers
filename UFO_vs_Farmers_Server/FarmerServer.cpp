#include "ServerSharedHeaders.h"

void FarmerServer::Update()
{
	float deltaTime{ 0 };
	sf::Vector2f oldPosition = sprite.getPosition();;
	//if so, is there a move we haven't processed yet?
	ClientProxyPtr client = NetworkManagerServer::sInstance->GetClientProxy(GetPlayerId());
	if (client)
	{
		MoveList& moveList = client->GetUnprocessedMoveList();
		for (const Move& unprocessedMove : moveList)
		{
			const InputState& currentState = unprocessedMove.GetInputState();
			deltaTime = unprocessedMove.GetDeltaTime();
			ServerProcessInput(deltaTime, currentState);
			//process input for gun
			std::static_pointer_cast<GunServer> (m_farmersGun)->ServerProcessInput(deltaTime, currentState);
		}

		moveList.Clear();
	}

	//this will take care of pisitioning the sprite after the movement was processed
	FarmerObject::Update();
	FarmerObject::checkAndApplyCollision();
	if (oldPosition == sprite.getPosition())
	{
		//std::cout << "same position" << std::endl;
	}
	else
	{
		NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), FRMR_PositionChange);
		NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), FRMR_SpriteChanged);

		if (m_isLeft)
		{
			m_farmersGun->sprite.setPosition(sprite.getPosition().x + 27, sprite.getPosition().y + 65);
		}
		else
		{
			m_farmersGun->sprite.setPosition(sprite.getPosition().x + 43, sprite.getPosition().y + 65);
		}

		NetworkManagerServer::sInstance->SetStateDirty(m_farmersGun->GetNetworkId(), m_farmersGun->EGUN_PositionChange);

	}
}

void FarmerServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject(this);
}

ClassIdentification FarmerServer::class_identification()
{
	return ClassIdentification::FarmerServer;
}

FarmerServer::FarmerServer()
{
}

void FarmerServer::ServerProcessInput(float elapsed, const InputState & inInputState)
{
	if (inInputState.is_D_pressed() && !m_disableMoveRight)
	{
		m_isLeft = { false };
		collisionRect.move(1 * m_move_speed, 0);

		m_timeSinceLastFrame += elapsed*1000;
		if (m_timeSinceLastFrame > 100)
		{
			m_counter++;
			m_counter %= 5;
			m_timeSinceLastFrame = 0;
		}
	}
	else
	{
		if (!m_isLeft)
		{
			m_counter = 0;
		}
	}

	if (inInputState.is_A_pressed() && !m_disableMoveLeft)
	{
		m_isLeft = { true };
		collisionRect.move(-1 * m_move_speed, 0);

		m_timeSinceLastFrame += elapsed*1000;
		if (m_timeSinceLastFrame > 100)
		{
			m_counter++;
			m_counter %= 5;
			m_timeSinceLastFrame = 0;
		}
	}
	else
	{
		if (m_isLeft)
		{
			m_counter = 5;
		}
	}

}
