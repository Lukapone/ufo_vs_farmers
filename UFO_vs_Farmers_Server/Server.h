class CowCounterServer;

class Server : public Engine
{
public:

	static bool StaticInit();
	virtual void DoFrame() override;
	virtual int Run();
	void HandleNewClientWithUFO( ClientProxyPtr inClientProxy, std::string & name);
	void HandleLostClient( ClientProxyPtr inClientProxy );
	void KillDisconectedUFOorFarmer(int inPlayerId);
	void SpawnUFOForPlayer( int inPlayerId, std::string & name);
	void HandleNewClientWithFarmer(ClientProxyPtr inClientProxy);
	void SpawnFarmerForClient(int inPlayerId);
	virtual void processEvents() override;
	void render();
	std::shared_ptr<CowCounter> mCowCounter;
	void loadHighestScore();
	void updateHighScore(int score, std::string & name);

private:
	Server();
	bool	InitNetworkManager();
	void	SetupWorld();
	std::string m_nameOfPlayerHighScore{ "noName" };
	uint32_t m_highestPlayerScore{ 0 };
};