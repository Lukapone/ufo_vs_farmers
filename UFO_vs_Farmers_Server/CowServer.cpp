#include "ServerSharedHeaders.h"

uint32_t CowServer::Write(OutputMemoryBitStream & inOutputStream, uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	if (inDirtyState & ECOW_Position)
	{
		inOutputStream.Write((bool)true);
		uint32_t posX = sprite.getPosition().x;
		uint32_t posY = sprite.getPosition().y;
		inOutputStream.Write(posX, 12);
		inOutputStream.Write(posY, 12);
		writtenState |= ECOW_Position;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	return writtenState;
}

ClassIdentification CowServer::class_identification()
{
	return ClassIdentification::CowServerObject;
}

void CowServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject(this);
}

CowServer::CowServer()
{
}
