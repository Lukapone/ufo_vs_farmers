#include "ServerSharedHeaders.h"

void BulletServer::ServerProcessInput(float elapsed, const InputState & inInputState)
{
}

void BulletServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject(this);
}

void BulletServer::Update()
{
	sprite.setPosition(collisionRect.getPosition());

	if (m_isMoving)
	{
		m_timeToDie += 1;
		if (m_timeToDie < 1000)
		{
			serverMove();
		}
		else
		{
			//std::cout << "bullet server died " << std::endl;
			m_isMoving = false;
			SetDoesWantToDie(true);
		}
	}

	checkAndApplyCollision();
}

void BulletServer::serverMove()
{
	sprite.move(sin(m_rotationInDegrees*3.14159265 / 180)*m_movementSpeed, cos(m_rotationInDegrees*3.14159265 / 180) * -m_movementSpeed);
	collisionRect.move(sin(m_rotationInDegrees*3.14159265 / 180)*m_movementSpeed, cos(m_rotationInDegrees*3.14159265 / 180) * -m_movementSpeed);
	/*sprite.move(sin(m_rotationInDegrees*3.14159265 / 180)*0.05, cos(m_rotationInDegrees*3.14159265 / 180) * -0.05);
	collisionRect.move(sin(m_rotationInDegrees*3.14159265 / 180)*0.05, cos(m_rotationInDegrees*3.14159265 / 180) * -0.05);*/
	NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), EBUL_PositionChange);
}

void BulletServer::checkAndApplyCollision()
{
	for (auto & gameObj : ObjectManagerV2::sInstance->getUpdateList())
	{
		if (gameObj->class_identification() == ClassIdentification::UFOServer)
		{
			if (gameObj->collisionRect.getGlobalBounds().intersects(this->collisionRect.getGlobalBounds()))
			{
				//std::cout << "damage " << std::endl;
				UFOServer & objUFO = dynamic_cast<UFOServer&>(*gameObj);
				objUFO.takeDamageServer(1);

				SetDoesWantToDie(true);
				

				if (objUFO.isDeath())
				{
					gameObj->SetDoesWantToDie(true);
					return;
				}
			}
		}
	}
}

BulletServer::BulletServer()
{
}

