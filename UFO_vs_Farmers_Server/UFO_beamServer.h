class UFO_beamServer : public UFO_beam
{
public:
	static GameObjectPtr StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new UFO_beamServer()); }

	virtual void Update();
	virtual uint32_t	Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;

	void checkAndApplyCollision(float elapsed, UFOServer * serverUFOPtr);
	virtual void HandleDying() override;

protected:
	UFO_beamServer();

private:
	

};

typedef std::shared_ptr<UFO_beamServer> UFO_beamServerPtr;