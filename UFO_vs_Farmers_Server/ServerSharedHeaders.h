
#include"UFO_Farmers_SharedHeaders.h"
#include<fstream>

#include "ReplicationManagerTransmissionData.h"
#include "ReplicationManagerServer.h"

#include "ClientProxy.h"
#include "NetworkManagerServer.h"
#include "Server.h"
#include "UFOServer.h"
#include "UFO_beamServer.h"
#include "CowServer.h"
#include "FarmerServer.h"

#include "CowCounterServer.h"
#include "GunServer.h"
#include "BulletServer.h"
