#include "ServerSharedHeaders.h"

GunServer::GunServer()
{
}

void GunServer::ServerProcessInput(float elapsed, const InputState & inInputState)
{
	if (inInputState.is_W_pressed())
	{
		m_rotationInDegrees++;
		m_rotationInDegrees %= 360;
		sprite.rotate(1);
		NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), EGUN_Rotation);
	}

	if (inInputState.is_S_pressed())
	{
		m_rotationInDegrees--;
		m_rotationInDegrees %= 360;
		sprite.rotate(-1);
		NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), EGUN_Rotation);
	}

	if (inInputState.is_C_pressed())
	{

		m_timeSinceLastShot += elapsed * 1000;
		if (m_timeSinceLastShot > 300)
		{
			severGunShoot();
			m_timeSinceLastShot = 0;
		}
	
	}
	else
	{
		m_timeSinceLastShot += elapsed * 1000;
	}

}

void GunServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject(this);
}

void GunServer::severGunShoot()
{
	std::shared_ptr<GameObject>firedBullet = GameObjectRegistry::sInstance->CreateGameObjectandAddToObjManager('BULL');
	BulletObject & objBullet = dynamic_cast<BulletObject&>(*firedBullet);
	objBullet.setPosition(sf::Vector2f(sprite.getPosition().x, sprite.getPosition().y));
	objBullet.setMovin(true);
	objBullet.setRotation(m_rotationInDegrees);
	objBullet.sprite.setRotation(m_rotationInDegrees);
	objBullet.SetSelfPointer(firedBullet);

}


