class BulletServer : public BulletObject
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new BulletServer()); }

	void ServerProcessInput(float elapsed, const InputState & inInputState);
	virtual void HandleDying() override;
	virtual void Update();
	void serverMove();
	void checkAndApplyCollision();
protected:
	BulletServer();
	int m_timeToDie{ 0 };
};