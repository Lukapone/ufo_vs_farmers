class GunServer : public GunObject
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new GunServer()); }

	void ServerProcessInput(float elapsed, const InputState & inInputState);
	virtual void HandleDying() override;
	void severGunShoot();
protected:
	GunServer();

};