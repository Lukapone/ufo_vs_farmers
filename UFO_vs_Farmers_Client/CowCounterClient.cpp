#include"ClientSharedHeaders.h"

void CowCounterClient::Read(InputMemoryBitStream & inInputStream)
{
	//std::cout << "read of the cow counter client" << std::endl;
	bool stateBit{ false };
	inInputStream.Read(stateBit);
	if (stateBit)
	{
		int32_t countRead = 0;

		inInputStream.Read(countRead, 5);
	
		m_cowCount = countRead;
		if (m_cowCount == 0)
		{
			std::cout << "read true:" << countRead << std::endl;
			static_cast<Client*> (Engine::sInstance.get())->m_renderVictoryScreenUFO = true;
		}
	}
}

CowCounterClient::CowCounterClient()
{
	text.setString( ": " + std::to_string(10));
}
