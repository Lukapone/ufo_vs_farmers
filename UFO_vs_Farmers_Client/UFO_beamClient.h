class UFO_beamClient : public UFO_beam
{
	public:
		static	GameObjectPtr	StaticCreate() { return GameObjectPtr(new UFO_beamClient()); }

		virtual void	Update();
		virtual void	Read(InputMemoryBitStream& inInputStream) override;

	protected:
		UFO_beamClient();


	private:
};