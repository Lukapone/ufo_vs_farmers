#include"ClientSharedHeaders.h"

FarmerClient::FarmerClient()
{
}

void FarmerClient::Update()
{
	
}

void FarmerClient::HandleDying()
{
	FarmerObject::HandleDying();
}

void FarmerClient::Read(InputMemoryBitStream & inInputStream)
{
	bool stateBit{ false };
	uint32_t readState = 0;

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		uint32_t playerId = 0;
		inInputStream.Read(playerId,8);
		SetPlayerId(playerId);
		readState |= FRMR_PlayerId;
	}

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		uint32_t position = 0;
		sf::Vector2f myReadPos;
		inInputStream.Read(position, 12);
		myReadPos.x = position;
		//std::cout << "X pos: " << position << std::endl;
		position = 0;
		inInputStream.Read(position, 12);
		myReadPos.y = position;
		//std::cout << "Y pos: " << position << std::endl;
		sprite.setPosition(myReadPos.x, myReadPos.y);
		collisionRect.setPosition(myReadPos.x, myReadPos.y);	
		readState |= FRMR_PositionChange;
	}

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		int32_t spriteCounter = 0;
		inInputStream.Read(spriteCounter, 3);
		//std::cout << "farmer sprite counter: " << spriteCounter << std::endl;
		m_counter = spriteCounter;
		inInputStream.Read(m_isLeft);
		sprite.setTextureRect(sf::IntRect(36 * m_counter, 46 * m_isLeft, 36, 46));
		readState |= FRMR_SpriteChanged;
	}

}


