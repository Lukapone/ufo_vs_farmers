class UFOClient : public UFO
{
public:
	static	GameObjectPtr	StaticCreate() { return GameObjectPtr(new UFOClient()); }

	virtual void	Update();
	virtual void	HandleDying() override;

	virtual void	Read(InputMemoryBitStream& inInputStream) override;

	virtual ClassIdentification class_identification() override;

protected:
	UFOClient();


private:

	float				mTimeLocationBecameOutOfSync;
	float				mTimeVelocityBecameOutOfSync;
};