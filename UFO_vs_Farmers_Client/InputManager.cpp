#include"ClientSharedHeaders.h"

std::unique_ptr< InputManager >	InputManager::sInstance;

namespace
{
	float kTimeBetweenInputSamples = 0.03f;
}

void InputManager::StaticInit()
{
	sInstance.reset( new InputManager() );
}


namespace
{
	inline void UpdateDesireVariableFromKey( EInputAction inInputAction, bool& ioVariable )
	{
		if( inInputAction == EIA_Pressed )
		{
			ioVariable = true;
		}
		else if( inInputAction == EIA_Released )
		{
			ioVariable = false;
		}
	}

	inline void UpdateDesireFloatFromKey( EInputAction inInputAction, float& ioVariable )
	{
		if( inInputAction == EIA_Pressed )
		{
			ioVariable = 1.f;
		}
		else if( inInputAction == EIA_Released )
		{
			ioVariable = 0.f;
		}
	}
}

void InputManager::HandleInput( EInputAction inInputAction, int inKeyCode )
{
	switch( inKeyCode )
	{
	case sf::Keyboard::W:
		UpdateDesireVariableFromKey(inInputAction, mCurrentState.mIs_W_pressed);
		break;
	case sf::Keyboard::S:
		UpdateDesireVariableFromKey(inInputAction, mCurrentState.mIs_S_pressed);
		break;
	case sf::Keyboard::A:
		UpdateDesireVariableFromKey(inInputAction, mCurrentState.mIs_A_pressed);
		break;
	case sf::Keyboard::D:
		UpdateDesireVariableFromKey(inInputAction, mCurrentState.mIs_D_pressed);
		break;
	case sf::Keyboard::C:
		UpdateDesireVariableFromKey( inInputAction, mCurrentState.mIs_C_pressed );
		break;
	}

}


InputManager::InputManager() :
	mNextTimeToSampleInput( 0.f ),
	mPendingMove( nullptr )
{

}

const Move& InputManager::SampleInputAsMove()
{
	return mMoveList.AddMove( GetState(), Timing::sInstance.GetFrameStartTime() );
}

bool InputManager::IsTimeToSampleInput()
{
	float time = Timing::sInstance.GetFrameStartTime();
	if( time > mNextTimeToSampleInput )
	{
		mNextTimeToSampleInput = mNextTimeToSampleInput + kTimeBetweenInputSamples;
		return true;
	}

	return false;
}

void InputManager::Update()
{
	if( IsTimeToSampleInput() )
	{
		mPendingMove = &SampleInputAsMove();
	}
}