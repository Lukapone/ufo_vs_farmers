class FarmerClient : public FarmerObject
{
public:
	static	GameObjectPtr	StaticCreate() { return GameObjectPtr(new FarmerClient()); }

	virtual void	Update();
	virtual void	HandleDying() override;

	virtual void	Read(InputMemoryBitStream& inInputStream) override;
protected:
	FarmerClient();


private:

	float				mTimeLocationBecameOutOfSync;
	float				mTimeVelocityBecameOutOfSync;
};