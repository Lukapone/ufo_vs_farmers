class CowCounterClient : public CowCounter
{
public:
	static	GameObjectPtr	StaticCreate() { return GameObjectPtr(new CowCounterClient()); }
	virtual void	Read(InputMemoryBitStream& inInputStream) override;
	
protected:
	CowCounterClient();

private:

};