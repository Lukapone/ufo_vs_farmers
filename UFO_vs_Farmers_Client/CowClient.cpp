#include"ClientSharedHeaders.h"

void CowClient::Read(InputMemoryBitStream & inInputStream)
{
	//std::cout << "read of the cow client" << std::endl;
	bool stateBit{ false };
	inInputStream.Read(stateBit);
	if (stateBit)
	{
		uint32_t position = 0;
		sf::Vector2f myReadPos;
		inInputStream.Read(position, 12);
		myReadPos.x = position;
		//std::cout << "X pos: " << position << std::endl;
		position = 0;
		inInputStream.Read(position, 12);
		myReadPos.y = position;
		//std::cout << "Y pos: " << position << std::endl;
		sprite.setPosition(myReadPos.x, myReadPos.y);
		collisionRect.setPosition(myReadPos.x, myReadPos.y);
	}
}

void CowClient::Update()
{

}

CowClient::CowClient()
{
}
