class GunClient : public GunObject
{
public:
	static	GameObjectPtr	StaticCreate() { return GameObjectPtr(new GunClient()); }
	virtual void	Read(InputMemoryBitStream& inInputStream) override;
	virtual void	Update();
protected:
	GunClient();

private:

};