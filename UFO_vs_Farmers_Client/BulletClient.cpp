#include"ClientSharedHeaders.h"

void BulletClient::Read(InputMemoryBitStream & inInputStream)
{
	bool stateBit{false};

	uint32_t readState = 0;

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		uint32_t position = 0;
		sf::Vector2f myReadPos;
		inInputStream.Read(position, 12);
		myReadPos.x = position;
		//std::cout << "X pos: " << position << std::endl;
		position = 0;
		inInputStream.Read(position, 12);
		myReadPos.y = position;
		//std::cout << "Y pos: " << position << std::endl;
		sprite.setPosition(myReadPos.x, myReadPos.y);
		collisionRect.setPosition(myReadPos.x, myReadPos.y);

		readState |= EBUL_PositionChange;

	}

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		uint32_t rotation = 0;
		inInputStream.Read(rotation, 9);
		//std::cout << "rot: " << rotation << std::endl;
		sprite.setRotation(rotation);
		collisionRect.setRotation(rotation);

		readState |= EBUL_Rotation;
	}
}

void BulletClient::Update()
{
}

BulletClient::BulletClient()
{
}
