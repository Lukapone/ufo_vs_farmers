class UFOClient;

class Client : public Engine
{
public:
	sf::Vector2f m_viewPosition;
	sf::View m_view_payer1;
	static bool StaticInit();
	void setClientUFOorFarmerPtr(GameObjectPtr ptrToClient, bool isUFO);
	bool m_renderVictoryScreenUFO{ false };
	bool m_renderVictoryScreenFarmer{ false };
	bool farmersCanWin{ false };
protected:

	Client();
	virtual void	DoFrame() override;
	virtual void processEvents() override;
	void render();
	void drawUFOVictoryScreen();
	void drawFarmerVictoryScreen();
	void drawRTT();
	void drawBG();
	void setBG();
	void setViewforFarmerOrUFO();
	void draBandWidth();

	void addBackground();
	bool checkForUFOsinObjectManager();
		
private:
	sf::Text mBandWidth;
	sf::Text mRTT;
	sf::Sprite m_BG;
	GameObjectPtr mClientUFOorFarmerPtr;
	bool m_isUFO{ true };
	bool m_alreadyLoaded{ false };
};