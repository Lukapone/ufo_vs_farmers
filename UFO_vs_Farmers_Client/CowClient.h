class CowClient : public CowObject
{
public:
	static	GameObjectPtr	StaticCreate() { return GameObjectPtr(new CowClient()); }
	virtual void	Read(InputMemoryBitStream& inInputStream) override;
	virtual void	Update();
protected:
	CowClient();

private:

};
