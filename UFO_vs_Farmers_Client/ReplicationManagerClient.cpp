#include"ClientSharedHeaders.h"

#include <cassert>

void ReplicationManagerClient::Read( InputMemoryBitStream& inInputStream )
{
	//!!!!!!!!!!!!!!!when optimize check different bit counts
	//while( inInputStream.GetRemainingBitCount() >= 10 && inInputStream.GetRemainingBitCount() <= 10000)
	//while (inInputStream.GetRemainingBitCount() >= 10)
	while( inInputStream.GetRemainingBitCount() >= 10)
	{
		//std::cout << "bit count remaining:" << inInputStream.GetRemainingBitCount() << std::endl;
		//read the network id... optimized to 8 bits
		int networkId = 0; 
		inInputStream.Read( networkId, 8 );
		
		//std::cout << "networkid:" << networkId << std::endl;
		//only need 2 bits for action...
		uint8_t action = 0; inInputStream.Read( action, 2 );

		//std::cout << "action:" << (int)action << std::endl;

		//std::cout << "reading action on the ccName: " << action << "with networkID: " << networkId << std::endl;
		switch( action )
		{
		case RA_Create:
			//std::cout << "reading create action on the id: "<< networkId << std::endl;
			ReadAndDoCreateAction( inInputStream, networkId );
			break;
		case RA_Update:
			//std::cout << "reading update action on the ccName: " << std::endl;
			ReadAndDoUpdateAction( inInputStream, networkId );
			break;
		case RA_Destroy:
			//std::cout << "reading destroy action on the id: " << networkId << std::endl;
			ReadAndDoDestroyAction( inInputStream, networkId );
			break;
		}

	}

}

void ReplicationManagerClient::ReadAndDoCreateAction( InputMemoryBitStream& inInputStream, int inNetworkId )
{

	//std::cout << "doing create action" << std::endl;
	//need 4 cc
	uint32_t fourCCName = 0;
	inInputStream.Read( fourCCName );

	//std::cout << "doing create action on the ccName: " << fourCCName << " with networkID: " << inNetworkId << std::endl;

	
	//we might already have this object- could happen if our ack of the create got dropped so server resends create request 
	//( even though we might have created )
	GameObjectPtr gameObject = NetworkManagerClient::sInstance->GetGameObject( inNetworkId );
	
	if( !gameObject )
	{
		//create the object and map it...
		
		gameObject = GameObjectRegistry::sInstance->CreateGameObjectandAddToObjManager( fourCCName );
		gameObject->SetNetworkId(inNetworkId);
		NetworkManagerClient::sInstance->AddToNetworkIdToGameObjectMap( gameObject );
		
		//it had really be the rigth type...
		assert( gameObject->GetClassId() == fourCCName );

		if (gameObject->GetClassId() == 'CUFO')
		{

			std::cout << "found the client setting it up ............................................................" << gameObject << std::endl;
			static_cast< Client* > (Engine::sInstance.get())->setClientUFOorFarmerPtr(gameObject, true);
		}
		if (gameObject->GetClassId() == 'FRMR')
		{
			std::cout << "found the client setting it up ............................................................" << gameObject << std::endl;
			static_cast< Client* > (Engine::sInstance.get())->setClientUFOorFarmerPtr(gameObject, false);
		}
		if (gameObject->GetClassId() == 'BULL')
		{
			SoundHolder::sInstance->play(SoundID::Gun);
		}
	}


	//and read state
	
	gameObject->Read( inInputStream );
}

void ReplicationManagerClient::ReadAndDoUpdateAction( InputMemoryBitStream& inInputStream, int inNetworkId )
{
	//LOG("need to update ReadAndDoUpdateAction", 0);
	//need object
	GameObjectPtr gameObject = NetworkManagerClient::sInstance->GetGameObject( inNetworkId );
	//std::cout << "updating gameobject with networkID: " << inNetworkId << std::endl;
	//gameObject MUST be found, because create was ack'd if we're getting an update... 
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//comment by lukas it is not always true if you get destroy action and than out of order update action it will break
	//and read state
	if (gameObject != nullptr)
	{
		gameObject->Read(inInputStream);
	}
}

void ReplicationManagerClient::ReadAndDoDestroyAction( InputMemoryBitStream& inInputStream, int inNetworkId )
{
	//LOG("need to update ReadAndDoDestroyAction", 0);
	//if something was destroyed before the create went through, we'll never get it
	//but we might get the destroy request, so be tolerant of being asked to destroy something that wasn't created
	//std::cout << "removing gameobject with networkID: " << inNetworkId << std::endl;
	GameObjectPtr gameObject = NetworkManagerClient::sInstance->GetGameObject( inNetworkId );
	if( gameObject )
	{
		//gameObject->SetDoesWantToDie( true );
		ObjectManagerV2::sInstance->Remove(gameObject);
		NetworkManagerClient::sInstance->RemoveFromNetworkIdToGameObjectMap( gameObject );
	}
}