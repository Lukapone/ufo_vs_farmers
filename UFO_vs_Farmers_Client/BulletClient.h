class BulletClient : public BulletObject
{
public:
	static	GameObjectPtr	StaticCreate() { return GameObjectPtr(new BulletClient()); }
	virtual void	Read(InputMemoryBitStream& inInputStream) override;
	virtual void	Update();

protected:
	BulletClient();

private:

};