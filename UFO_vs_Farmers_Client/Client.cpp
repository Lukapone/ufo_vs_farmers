#include"ClientSharedHeaders.h"

bool Client::StaticInit( )
{
	// Create the Client pointer first because it initializes SDL
	Client* client = new Client();
	InputManager::StaticInit();
	TextureHolder::StaticInit();
	TextureHolder::sInstance->loadTextures();
	FontHolder::StaticInit();
	FontHolder::sInstance->loadFonts();
	SoundHolder::StaticInit();
	SoundHolder::sInstance->loadSoundBuffers();

	sInstance.reset( client );
	
	GameObjectPtr myBG1 = std::make_shared<GameObject>();
	TextureHolder::sInstance->get(TextureID::Background).setRepeated(true);
	TextureHolder::sInstance->get(TextureID::Background).setSmooth(false);
	myBG1->sprite.setTexture(TextureHolder::sInstance->get(TextureID::Background));
	myBG1->sprite.setTextureRect(sf::IntRect(0, 0, 5120, 768));
	ObjectManagerV2::sInstance->Add(myBG1);


	GameObjectPtr myCloud = std::make_shared<GameObject>();
	TextureHolder::sInstance->get(TextureID::Cloud).setSmooth(false);
	myCloud->sprite.setTexture(TextureHolder::sInstance->get(TextureID::Cloud));
	myCloud->sprite.setPosition(50, 20);
	myCloud->sprite.setScale(0.5, 0.3);

	myCloud->collisionRect.setSize(sf::Vector2f(600, 442));
	myCloud->collisionRect.setPosition(myCloud->sprite.getPosition());
	myCloud->collisionRect.setScale(myCloud->sprite.getScale());

	ObjectManagerV2::sInstance->Add(myCloud);

	//add clowds into game
	std::shared_ptr<GameObject>toAdd;
	int cloudCount = 8;
	for (int i = 0; i < cloudCount; i++)
	{
		toAdd = myCloud->SharedClone();
		toAdd->collisionRect.setPosition(getRandomIntWithSeed(2 + i, 100, 4000), getRandomIntWithSeed(2 + i, 0, 200));
		toAdd->sprite.setPosition(toAdd->collisionRect.getPosition());
		ObjectManagerV2::sInstance->Add(toAdd);
	}

	GameObjectPtr myTree = std::make_shared<GameObject>();
	TextureHolder::sInstance->get(TextureID::Tree).setSmooth(false);

	myTree->sprite.setTexture(TextureHolder::sInstance->get(TextureID::Tree));
	myTree->sprite.setPosition(50, 330);
	myTree->sprite.setScale(0.5, 0.3);

	myTree->collisionRect.setSize(sf::Vector2f(600, 442));
	myTree->collisionRect.setPosition(myTree->sprite.getPosition());
	myTree->collisionRect.setScale(myTree->sprite.getScale());

	ObjectManagerV2::sInstance->Add(myTree);

	//add clowds into game
	std::shared_ptr<GameObject>toAddTree;
	int treeCount = 8;
	for (int i = 0; i < treeCount; i++)
	{
		toAddTree = myTree->SharedClone();
		toAddTree->collisionRect.setPosition(getRandomIntWithSeed(3 + i, 100, 4000), 330);
		toAddTree->sprite.setPosition(toAddTree->collisionRect.getPosition());
		ObjectManagerV2::sInstance->Add(toAddTree);
	}

	GameObjectPtr myBarn = std::make_shared<GameObject>();
	TextureHolder::sInstance->get(TextureID::Barn).setSmooth(false);
	myBarn->sprite.setTexture(TextureHolder::sInstance->get(TextureID::Barn));
	myBarn->sprite.setPosition(4000, 0);
	myBarn->sprite.setScale(1, 1);

	myBarn->collisionRect.setSize(sf::Vector2f(600, 588));
	myBarn->collisionRect.setPosition(myBarn->sprite.getPosition());
	myBarn->collisionRect.setScale(myBarn->sprite.getScale());

	ObjectManagerV2::sInstance->Add(myBarn);
	return true;
}

Client::Client()
{
	GameObjectRegistry::sInstance->RegisterCreationFunction('CUFO', UFOClient::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('BEAM', UFO_beamClient::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('CCOW', CowClient::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('CCNT', CowCounterClient::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('FRMR', FarmerClient::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('CGUN', GunClient::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('BULL', BulletClient::StaticCreate);

	std::string destination = "127.0.0.1:42000";
	//std::string destination = StringUtils::GetCommandLineArg(1);
	std::string name = "ClientName";
	//std::string name = StringUtils::GetCommandLineArg(2);
	//std::string number = StringUtils::GetCommandLineArg(3);
	//int wantsOne = atoi(number.c_str());
	//bool wantsUFO = wantsOne;
	bool wantsUFO = true;

	SocketAddressPtr serverAddress = SocketAddressFactory::CreateIPv4FromString( destination );
	NetworkManagerClient::StaticInit( *serverAddress, name, wantsUFO);
	mWindow.setTitle("CLIENT WINDOW");
	m_view_payer1.reset(sf::FloatRect(0, 0, mWindow.getSize().x, mWindow.getSize().y));
	m_view_payer1.setViewport(sf::FloatRect(0, 0, 1.0f, 1.0f));
	bool isGunPlaying = false;
}



void Client::DoFrame()
{
	InputManager::sInstance->Update();

	Engine::DoFrame();

	NetworkManagerClient::sInstance->ProcessIncomingPackets();

	checkForUFOsinObjectManager();
	if (farmersCanWin == true)
	{
		m_renderVictoryScreenFarmer = checkForUFOsinObjectManager();
	}
	render();

	NetworkManagerClient::sInstance->SendOutgoingPackets();
}

void Client::processEvents()
{
	sf::Event event;
	while (mWindow.pollEvent(event))
	{
		switch (event.type)
		{
			case sf::Event::KeyPressed:
				InputManager::sInstance->HandleInput(EIA_Pressed,event.key.code);
				break;
			case sf::Event::KeyReleased:
				InputManager::sInstance->HandleInput(EIA_Released, event.key.code);
				break;
			case sf::Event::Closed:
				mWindow.close();
				break;
		}

	}
}

void Client::render()
{
		if (m_renderVictoryScreenUFO == true)
		{
			m_viewPosition.y = mWindow.getSize().y / 2;
			m_viewPosition.x = mWindow.getSize().x / 2;
			m_view_payer1.setCenter(m_viewPosition);
			mWindow.setView(m_view_payer1);
			drawUFOVictoryScreen();
		}
		else if (m_renderVictoryScreenFarmer == true)
		{
			m_viewPosition.y = mWindow.getSize().y / 2;
			m_viewPosition.x = mWindow.getSize().x / 2;
			m_view_payer1.setCenter(m_viewPosition);
			mWindow.setView(m_view_payer1);
			drawFarmerVictoryScreen();
		}
		else
		{
			setViewforFarmerOrUFO();
			int drawnObjects = ObjectManagerV2::sInstance->Draw(mWindow);
			drawRTT();
			draBandWidth();
		}

	mWindow.display();
	mWindow.clear();
}

void Client::drawUFOVictoryScreen()
{
	mRTT.setFont(FontHolder::sInstance->get(FontID::Arial));
	mRTT.setCharacterSize(60);
	mRTT.setColor(sf::Color::Blue);
	mRTT.setPosition(300, 300);
	mRTT.setString("UFO's WON");
	mWindow.draw(mRTT);
}

void Client::drawFarmerVictoryScreen()
{
	mRTT.setFont(FontHolder::sInstance->get(FontID::Arial));
	mRTT.setCharacterSize(60);
	mRTT.setColor(sf::Color::Blue);
	mRTT.setPosition(300, 300);
	mRTT.setString("Farmer's WON");
	mWindow.draw(mRTT);
}

void Client::drawRTT()
{
	mRTT.setFont(FontHolder::sInstance->get(FontID::Arial));
	mRTT.setCharacterSize(30);
	mRTT.setColor(sf::Color::Red);
	mRTT.setPosition(10, 10);
	float rttMS = NetworkManagerClient::sInstance->GetAvgRoundTripTime().GetValue() * 1000.f;
	std::string roundTripTime = StringUtils::Sprintf("RTT %d ms", (int)rttMS);
	mRTT.setString(roundTripTime);
	mWindow.draw(mRTT);
}

void Client::drawBG()
{
	if (!m_alreadyLoaded)
	{
		m_alreadyLoaded = true;
		setBG();
	}

	mWindow.draw(m_BG);
}

void Client::setBG()
{
	m_BG.setTexture(TextureHolder::sInstance->get(TextureID::Background));
	m_BG.setPosition(sf::Vector2f(0, 0));
}



void Client::setViewforFarmerOrUFO()
{

	//the checks are for ufo or farmer different screen scrolling settings
	if (m_isUFO && mClientUFOorFarmerPtr != nullptr)
	{
		//update the viewport
		if (mClientUFOorFarmerPtr->sprite.getPosition().x > mWindow.getSize().x / 2)
		{
			m_viewPosition.x = mClientUFOorFarmerPtr->sprite.getPosition().x;
		}
		else
		{
			m_viewPosition.x = mWindow.getSize().x / 2;
		}

		if (mClientUFOorFarmerPtr->sprite.getPosition().y > mWindow.getSize().y / 2)
		{
			m_viewPosition.y = mClientUFOorFarmerPtr->sprite.getPosition().y;
		}
		else
		{
			//viewPosition.y = myUFO->collisionRect.getPosition().y;
			m_viewPosition.y = mWindow.getSize().y / 2;
		}
		m_view_payer1.setCenter(m_viewPosition);
		mWindow.setView(m_view_payer1);
	}

	if (!m_isUFO && mClientUFOorFarmerPtr != nullptr)
	{
		//update the viewport
		if (mClientUFOorFarmerPtr->sprite.getPosition().x > mWindow.getSize().x / 2)
		{
			m_viewPosition.x = mClientUFOorFarmerPtr->sprite.getPosition().x;
		}
		else
		{
			m_viewPosition.x = mWindow.getSize().x / 2;
		}
		m_viewPosition.y = mWindow.getSize().y / 2;
		m_view_payer1.setCenter(m_viewPosition);
		mWindow.setView(m_view_payer1);
	}

	int drawnObjects = ObjectManagerV2::sInstance->Draw(mWindow);
	drawRTT();
	draBandWidth();
	mWindow.display();
	mWindow.clear();
}

void Client::draBandWidth()
{
	mBandWidth.setFont(FontHolder::sInstance->get(FontID::Arial));
	mBandWidth.setCharacterSize(30);
	mBandWidth.setColor(sf::Color::Red);
	mBandWidth.setPosition(10, 35);
	std::string bandwidth = StringUtils::Sprintf("In %d  Bps, Out %d Bps",
		static_cast< int >(NetworkManagerClient::sInstance->GetBytesReceivedPerSecond().GetValue()),
		static_cast< int >(NetworkManagerClient::sInstance->GetBytesSentPerSecond().GetValue()));
	mBandWidth.setString(bandwidth);
	mWindow.draw(mBandWidth);
}

void Client::addBackground()
{
	std::shared_ptr<GameObject>myBG = std::make_shared<GameObject>();
	//crashes here
	myBG->sprite.setTexture(TextureHolder::sInstance->get(TextureID::Background));
	myBG->sprite.setTextureRect(sf::IntRect(0, 0, 5120, 768));
	myBG->collisionRect.setSize(sf::Vector2f(4096, 768));
	myBG->sprite.setPosition(0, 0);
}

bool Client::checkForUFOsinObjectManager()
{
	for (auto & gameObj : ObjectManagerV2::sInstance->getUpdateList())
	{
		if (gameObj->class_identification() == ClassIdentification::UFOClient)
		{
			farmersCanWin = true;
			return false;
		}
	}
	return true;
}

void Client::setClientUFOorFarmerPtr(GameObjectPtr ptrToClient, bool isUFO)
{
	if (mClientUFOorFarmerPtr == nullptr)
	{
		m_isUFO = isUFO;
		mClientUFOorFarmerPtr = ptrToClient;
	}
}

