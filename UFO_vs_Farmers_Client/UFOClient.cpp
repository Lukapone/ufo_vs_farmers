#include"ClientSharedHeaders.h"

void UFOClient::Update()
{
}

void UFOClient::HandleDying()
{
	UFO::HandleDying();
}

void UFOClient::Read(InputMemoryBitStream & inInputStream)
{
	bool stateBit{ false };

	uint32_t readState = 0;

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		uint32_t playerId = 0;
		inInputStream.Read(playerId,8);
		SetPlayerId(playerId);
		inInputStream.Read(m_PlayerName);
		readState |= ECRS_PlayerId;
	}

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		uint32_t position = 0;
		sf::Vector2f myReadPos;
		inInputStream.Read(position,12);		
		myReadPos.x = position;
		//std::cout << "X pos: " << position << std::endl;
		position = 0;
		inInputStream.Read(position,12);
		myReadPos.y = position;
		//std::cout << "Y pos: " << position << std::endl;
		sprite.setPosition(myReadPos.x, myReadPos.y);
		collisionRect.setPosition(myReadPos.x, myReadPos.y);
		text.setPosition(myReadPos.x - 50, myReadPos.y - 100);
		readState |= ECRS_PositionChange;

	}

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		m_health = 0;
		inInputStream.Read(m_health, 5);
		//std::cout << "health: " << m_health << std::endl;
		text.setString(m_PlayerName + ": " + std::to_string(m_health));
		readState |= ECRS_Health;
	}

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		uint32_t rotation = 0;
		inInputStream.Read(rotation,9);
		//std::cout << "rot: " << rotation << std::endl;
		sprite.setRotation(rotation);
		collisionRect.setRotation(rotation);
		readState |= ECRS_Rotation;
	}

}

ClassIdentification UFOClient::class_identification()
{
	return ClassIdentification::UFOClient;
}

UFOClient::UFOClient()
{
	m_PlayerName = NetworkManagerClient::sInstance->getPlayerName();
	text.setString(m_PlayerName + ": " + std::to_string(m_health));
}
