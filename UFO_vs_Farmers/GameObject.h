#pragma once


#define CLASS_IDENTIFICATION( inCode, inClass ) \
enum { kClassId = inCode }; \
virtual uint32_t GetClassId() const { return kClassId; } \
static GameObject* CreateInstance() { return static_cast< GameObject* >( new inClass() ); } \

//#include"ObjectManagerV2.h"
enum class ClassIdentification;
class ObjectManagerV2;
class OutputMemoryBitStream;
class InputMemoryBitStream;

class GameObject
{
private:
	int mNetworkId{-1};
	bool mDoesWantToDie{false};

protected:
	int32_t mIndexInWorld{ -1 };
	int8_t m_ZIndex{ 0 };

public:
	CLASS_IDENTIFICATION('GOBJ', GameObject)

	sf::Sprite sprite;
	sf::RectangleShape collisionRect;
	sf::Text text;

	void	SetIndexInWorld(int inIndex) { mIndexInWorld = inIndex; }
	int			GetNetworkId()				const { return mNetworkId; }
	void		SetNetworkId(int inNetworkId);

	virtual uint32_t	Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const { (void)inOutputStream; (void)inDirtyState; return 0; }
	virtual void		Read(InputMemoryBitStream& inInputStream) { (void)inInputStream; }

	virtual uint32_t GetAllStateMask()	const { return 0; }


	bool		DoesWantToDie()				const { return mDoesWantToDie; }
	void		SetDoesWantToDie(bool inWants) { mDoesWantToDie = inWants; }
	

	//this is objectManager in game and is static so only one instance will be alocated in memory dont forget to initialize in .cpp file or linker will report eror
	static ObjectManagerV2 *objectManager;

	int8_t getZIndex() const;
	void setZIndex(int8_t toChange);

	int		GetIndexInWorld()				const { return mIndexInWorld; }


	virtual ~GameObject() {}

	virtual void Update(sf::Time sinceStart, sf::Time elapsed);

	virtual void Update();

	virtual ClassIdentification class_identification();

	virtual std::unique_ptr<GameObject> Clone();

	virtual std::shared_ptr<GameObject> SharedClone();

	virtual void HandleDying() {}


};


typedef std::shared_ptr< GameObject >	GameObjectPtr;




