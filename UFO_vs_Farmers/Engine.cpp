#include"UFO_FarmersShared.h"

#include <time.h>


std::unique_ptr< Engine >	Engine::sInstance;


Engine::Engine() :
	mShouldKeepRunning(true),mWindow(sf::VideoMode(800, 600), "UFO vs Farmers")
{
	SocketUtil::StaticInit();

	srand(static_cast< uint32_t >(time(nullptr)));


	GameObjectRegistry::StaticInit();


	ObjectManagerV2::StaticInit();

	//ScoreBoardManager::StaticInit();

	//SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);
}

Engine::~Engine()
{
	SocketUtil::CleanUp();

	//SDL_Quit();
}




int Engine::Run()
{
	return DoRunLoop();
}

void Engine::processEvents()
{
	LOG("Engine process events called should be overriden", 0)
}

void Engine::render()
{
	mWindow.clear();
	ObjectManagerV2::sInstance->Draw(mWindow);
	//mWindow.draw();
	mWindow.display();
}

int Engine::DoRunLoop()
{
	// Main message loop
	bool quit = false;

	while (mWindow.isOpen())
	{
		
		//sample events for keyboard so the packets will be send
		processEvents();

		Timing::sInstance.Update();

		DoFrame();
		
	}

	return 0;
}

void Engine::DoFrame()
{
	//std::cout << "doing base_engine frame" << std::endl;
	//World::sInstance->Update();
	ObjectManagerV2::sInstance->Update();
}


