#pragma once

enum class ClassIdentification 
{
	GameObject,
	UFO,
	UFO_beam,
	CowObject,
	CowServerObject,
	FarmerObject,
	BulletObject,
	GunObject,
	UFOServer,
	UFOClient,
	FarmerServer
};