#include"UFO_FarmersShared.h"

ObjectManagerV2 *GameObject::objectManager;

void GameObject::SetNetworkId(int inNetworkId)
{
	//this doesn't put you in the map or remove you from it
	mNetworkId = inNetworkId;

}

int8_t GameObject::getZIndex() const
{
	return m_ZIndex;
}

void GameObject::setZIndex(int8_t toChange)
{
	m_ZIndex = toChange;
}

void GameObject::Update(sf::Time sinceStart, sf::Time elapsed)
{
}

void GameObject::Update()
{
	//calls update on derived classes
}

ClassIdentification GameObject::class_identification()
{
	return ClassIdentification::GameObject;
}

//deep cloning
std::unique_ptr<GameObject> GameObject::Clone()
{
	return  std::make_unique<GameObject>(*this);
}

std::shared_ptr<GameObject> GameObject::SharedClone()
{
	return  std::make_shared<GameObject>(*this);
}


