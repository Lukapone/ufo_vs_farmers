#include"UFO_FarmersShared.h"

namespace
{
	void WriteSignedBinaryValue( OutputMemoryBitStream& inOutputStream, float inValue )
	{
		bool isNonZero = ( inValue != 0.f );
		inOutputStream.Write( isNonZero );
		//std::cout << "writting is nonzero: " << isNonZero << std::endl;
		if( isNonZero )
		{
			inOutputStream.Write( inValue > 0.f );
			//std::cout << "writting is positive: " << std::endl;
		}
	}

	void ReadSignedBinaryValue( InputMemoryBitStream& inInputStream, float& outValue )
	{
		bool isNonZero;
		inInputStream.Read( isNonZero );
		//std::cout << "reding is nonzero: " << isNonZero << std::endl;
		if( isNonZero )
		{
			bool isPositive;
			inInputStream.Read( isPositive );
			//std::cout << "reding is positive: " << isPositive << std::endl;
			outValue = isPositive ? 1.f : -1.f;
		}
		else
		{
			outValue = 0.f;
		}
	}
}

InputState::InputState()
{
}

bool InputState::Write( OutputMemoryBitStream& inOutputStream ) const
{
	//WriteSignedBinaryValue( inOutputStream, GetDesiredHorizontalDelta() );
	//std::cout << "Input state writing: horizontal delta: " << GetDesiredHorizontalDelta() << std::endl;
	//WriteSignedBinaryValue( inOutputStream, GetDesiredVerticalDelta() );
	//inOutputStream.Write( mIsShooting );
	//std::cout << "writting is shooting: " << mIsShooting << std::endl;

	inOutputStream.Write(mIs_W_pressed);
	inOutputStream.Write(mIs_S_pressed);
	inOutputStream.Write(mIs_A_pressed);
	inOutputStream.Write(mIs_D_pressed);
	inOutputStream.Write(mIs_C_pressed);

	//std::cout << "9.Write input state: 5bits = " << inOutputStream.GetBitLength() << std::endl;
	return false;
}

bool InputState::Read( InputMemoryBitStream& inInputStream )
{
	
	//ReadSignedBinaryValue( inInputStream, mDesiredRightAmount );
	////std::cout << "mDesiredRightAmount: " << mDesiredRightAmount << std::endl;
	//ReadSignedBinaryValue( inInputStream, mDesiredUpAmount );
	////std::cout << "mDesiredUpAmount: " << mDesiredUpAmount << std::endl;
	//inInputStream.Read( mIsShooting );
	////std::cout << "mIsShooting: " << mIsShooting << std::endl;
	inInputStream.Read(mIs_W_pressed);
	inInputStream.Read(mIs_S_pressed);
	inInputStream.Read(mIs_A_pressed);
	inInputStream.Read(mIs_D_pressed);
	inInputStream.Read(mIs_C_pressed);

	return true;
}