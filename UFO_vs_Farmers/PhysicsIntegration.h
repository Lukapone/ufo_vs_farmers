#pragma once

struct ObjectState
{
	 sf::Vector2f position;
	 sf::Vector2f velocity;
	 sf::Vector2f acceleration;
};


void accelerate(ObjectState &state, float t);

void accelerate_y(ObjectState &state, float t);

void accelerate_x(ObjectState &state, float t);

void deaccelerate(ObjectState &state, float t);

void deaccelerate_y(ObjectState &state, float t);

void deaccelerate_x(ObjectState &state, float t);

void move(ObjectState &state, float t);

