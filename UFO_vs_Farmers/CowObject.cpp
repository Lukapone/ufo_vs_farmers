#include"UFO_FarmersShared.h"

CowObject::CowObject()
{
	m_ZIndex = 2;

	sprite.setTexture(TextureHolder::sInstance->get(TextureID::Cow));
	collisionRect.setSize(sf::Vector2f(512, 289));
	collisionRect.setFillColor(sf::Color::Red);
	collisionRect.setScale(0.2, 0.2);
	collisionRect.setPosition(300, 510);//this is in centre of screen needs to be adapted to diferent resolutions
	sprite.setScale(collisionRect.getScale());
}


void CowObject::Update()
{
	//std::cout << "base cow update" << std::endl;
	//sprite.setPosition(collisionRect.getPosition());
	collisionRect.setPosition(sprite.getPosition());


}

void CowObject::Update(sf::Time sinceStart, sf::Time elapsed)
{
	sprite.setPosition(collisionRect.getPosition());
	sprite.setScale(collisionRect.getScale());
	checkAndApplyCollision(sinceStart);
	HandleInput();

}

ClassIdentification CowObject::class_identification()
{
	return ClassIdentification::CowObject;
}

std::unique_ptr<GameObject> CowObject::Clone()
{
	return std::make_unique<CowObject>(*this);
}

std::shared_ptr<GameObject> CowObject::SharedClone()
{
	return std::make_shared<CowObject>(*this);
}


void CowObject::HandleInput()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::U))
	{
		collisionRect.move(0, -1 * m_movementSpeed);
		//sf::Vector2<float> myScale(0.5,0.5);
		//spriteUFO.scale(myScale);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::J))
	{
		collisionRect.move(0, 1 * m_movementSpeed);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::H))
	{
		collisionRect.move(-1 * m_movementSpeed, 0);
		//sf::Vector2<float> myScale(0.5,0.5);
		//spriteUFO.scale(myScale);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::K))
	{
		collisionRect.move(1 * m_movementSpeed, 0);
	}

}

void CowObject::takeDamege(int32_t amount)
{
	m_health -= amount;
}

//made by Lukas where the cows are spaced evenly at the beginning of the game if they collide with eachother
void CowObject::checkAndApplyCollision(sf::Time sinceStart)
{
	for (auto & gameObj : GameObject::objectManager->getUpdateList())
	{
		if (gameObj->class_identification() == ClassIdentification::CowObject && gameObj->collisionRect.getPosition() != this->collisionRect.getPosition())
		{
			if (gameObj->collisionRect.getGlobalBounds().intersects(this->collisionRect.getGlobalBounds()))
			{

				//std::cout << "cows on same spot " << std::endl;
				gameObj->collisionRect.setPosition(getRandomMt19937(100, 4000), 510);
				//collisionRect.move(getRandomMt19937(-1, 1), 0);
				/*std::shared_ptr<GameObject>toAdd = gameObj->SharedClone();
				toAdd->collisionRect.setPosition(getRandomMt19937(100, 4000), 510);
				GameObject::objectManager->Add(toAdd);
				GameObject::objectManager->Remove(gameObj)*/;
				return;
			}
		}
	}
}
	


inline int32_t CowObject::getHealth() const
{
	return m_health;
}

bool CowObject::isDeath() const
{
	if (m_health <= 0)
	{
		return true;
	}

	return false;
}

//uint32_t CowObject::Write(OutputMemoryBitStream & inOutputStream, uint32_t inDirtyState) const
//{
//	uint32_t writtenState = 0;
//
//	if (inDirtyState & EMRS_Position)
//	{
//		inOutputStream.Write((bool)true);
//		inOutputStream.Write(sprite.getPosition().x);
//		inOutputStream.Write(sprite.getPosition().y);
//		writtenState |= EMRS_Position;
//	}
//	else
//	{
//		inOutputStream.Write((bool)false);
//	}
//
//	return writtenState;
//}

//void CowObject::Read(InputMemoryBitStream & inInputStream)
//{
//	
//}


