#pragma once

template <typename Resource, typename Identifier>
class ResourceHolder
{
public:

	void load(Identifier id, const std::string& filename);
	Resource& get(Identifier id);
	const Resource& get(Identifier id) const;
private:
	std::map<Identifier, std::unique_ptr<Resource >> mResourceMap;
};

template<typename Resource, typename Identifier>
inline void ResourceHolder<Resource, Identifier>::load(Identifier id, const std::string & filename)
{
	std::unique_ptr<Resource> resource(std::make_unique<Resource>());
	if (!resource->loadFromFile(filename)) 
		throw std::runtime_error("ResourceHolder::load - Failed to load " + filename);

	auto inserted = mResourceMap.insert( std::make_pair(id, std::move(resource)));
	assert(inserted.second);
}

template<typename Resource, typename Identifier>
inline Resource & ResourceHolder<Resource, Identifier>::get(Identifier id)
{
	auto found = mTextureMap.find(id);
	return *found->second;
}

template<typename Resource, typename Identifier>
inline const Resource & ResourceHolder<Resource, Identifier>::get(Identifier id) const
{
	const auto found = mTextureMap.find(id);
	return *found->second;
}
