#pragma once

constexpr float GB_BULLET_MOVEMENT_SPEED = 1;
constexpr int GB_BULLET_ROTATION_IN_DEGREES = 45;
constexpr int GB_BULLET_TIME_SINCE_FIRED = 0;
constexpr int GB_BULLET_TIME_PRESSED = 0;
constexpr bool GB_BULLET_IS_MOVING = false;

constexpr float GB_COW_MOVEMENT_SPEED = 0.5f;
constexpr int32_t GB_COW_HEALTH = 10;

constexpr float GB_FARMER_MOVE_SPEED{ 2.5f };
constexpr int GB_FARMER_COUNTER{ 0 };
constexpr int GB_FARMER_TIME_SINCE_LAST_FRAME{ 0 };
constexpr bool GB_FARMER_IS_LEFT{ false };
constexpr bool GB_FARMER_DISABLE_MOVE_RIGHT{ false };
constexpr bool GB_FARMER_DISBALE_MOVE_LEFT{ false };

constexpr int GB_GUN_TIME_SINCE_LAST_ROTATE { 0 };
constexpr bool GB_GUN_SHOT_LOCK{ false };
constexpr int GB_GUN_ROTATION_IN_DEGREES{ 45 };
constexpr int GB_GUN_TIME_SINCE_LAST_SHOT{ 0 };

constexpr float GB_UFO_MAX_VELOCITY = 700;
constexpr float GB_UFO_ACCELERATION_SIDES = 1000;
constexpr float GB_UFO_ACCLERATION_UP_DOWN = 100;