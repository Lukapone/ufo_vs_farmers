#include"UFO_FarmersShared.h"

FarmerObject::FarmerObject()
{
	sprite.setTexture(TextureHolder::sInstance->get(TextureID::Farmer));
	sprite.setTextureRect(sf::IntRect(0, 0, 35, 46));
	sprite.setPosition(300, 475);
	sprite.setScale(2, 2);

	collisionRect.setSize(sf::Vector2f(100, 100));
	collisionRect.setFillColor(sf::Color::Green);
	collisionRect.setScale(2, 2);
	collisionRect.setPosition(300, 475);
	m_ZIndex = 1;
	
}

void FarmerObject::Update(sf::Time sinceStart, sf::Time elapsed)
{
	sprite.setTextureRect(sf::IntRect(36 * m_counter,46 * m_isLeft,36,46));
	sprite.setPosition(collisionRect.getPosition());
	sprite.setScale(collisionRect.getScale());
	collisionRect.setPosition(sprite.getPosition());
	

	//sets which arm the gun is on depending on what way the player is facing
	if (m_isLeft)
	{
		m_farmersGun->sprite.setPosition(sprite.getPosition().x + 27, sprite.getPosition().y + 65);
	}
	else
	{
		m_farmersGun->sprite.setPosition(sprite.getPosition().x + 43, sprite.getPosition().y + 65);
	}

	HandleInput(sinceStart, elapsed);
	checkAndApplyCollision();
}

void FarmerObject::Update()
{
	//std::cout << "base farmer need recode update" << std::endl;
	sprite.setTextureRect(sf::IntRect(36 * m_counter, 46 * m_isLeft, 36, 46));
	sprite.setPosition(collisionRect.getPosition());
	collisionRect.setPosition(sprite.getPosition());

}

ClassIdentification FarmerObject::class_identification()
{
	return ClassIdentification::FarmerObject;
}

std::unique_ptr<GameObject> FarmerObject::Clone()
{
	return std::make_unique<FarmerObject>(*this);
}

std::shared_ptr<GameObject> FarmerObject::SharedClone()
{
	return std::make_shared<FarmerObject>(*this);
}

//after 100 milliseconds the animation will fliop to the next frame, loopable and reverses when the player changes directions. also goes to first frame for when player is idle - made by Paddy
void FarmerObject::HandleInput(sf::Time sinceStart, sf::Time elapsed)
{
	
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && !m_disableMoveRight)
	{
		m_isLeft = { false };
		collisionRect.move(1 * m_move_speed, 0);

		m_timeSinceLastFrame += elapsed.asMilliseconds();
		if (m_timeSinceLastFrame > 100)
		{
			m_counter++;
			m_counter %= 5;
			m_timeSinceLastFrame = 0;
		}
	}
	else
	{
		if (!m_isLeft)
		{
			m_counter = 0;
		}
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && !m_disableMoveLeft)
	{
		m_isLeft = { true };
		collisionRect.move(-1 * m_move_speed, 0);

		m_timeSinceLastFrame += elapsed.asMilliseconds();
		if (m_timeSinceLastFrame > 100)
		{
			m_counter++;
			m_counter %= 5;
			m_timeSinceLastFrame = 0;
		}
	}
	else
	{
		if (m_isLeft)
		{
			m_counter = 5;
		}
	}
}

void FarmerObject::checkAndApplyCollision()
{
	if (collisionRect.getPosition().x < 0)
	{
		std::cout << "below zero x " << std::endl;
		m_disableMoveLeft = true;
	}
	else
	{
		m_disableMoveLeft = false;
	}

	if (collisionRect.getPosition().x > 4068)
	{
		m_disableMoveRight = true;
		std::cout << "abowe map border" << std::endl;
	}
	else
	{
		m_disableMoveRight = false;
	}

}

uint32_t FarmerObject::Write(OutputMemoryBitStream & inOutputStream, uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	if (inDirtyState & FRMR_PlayerId)
	{
		inOutputStream.Write((bool)true);
		inOutputStream.Write(GetPlayerId(),8);

		writtenState |= FRMR_PlayerId;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}


	if (inDirtyState & FRMR_PositionChange)
	{
		inOutputStream.Write((bool)true);
		uint32_t posX = sprite.getPosition().x;
		uint32_t posY = sprite.getPosition().y;
		inOutputStream.Write(posX, 12);
		inOutputStream.Write(posY, 12);

		writtenState |= FRMR_PositionChange;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	if (inDirtyState & FRMR_SpriteChanged)
	{
		inOutputStream.Write((bool)true);

		//depending on how many poses there is
		inOutputStream.Write(m_counter,3);
		
		inOutputStream.Write(m_isLeft);

		writtenState |= FRMR_SpriteChanged;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}
	

	//std::cout << "after using WRITE the writtenstate is : " << writtenState << std::endl;
	return writtenState;
}
