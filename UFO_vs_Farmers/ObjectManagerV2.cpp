#include"UFO_FarmersShared.h"

std::unique_ptr< ObjectManagerV2 >	ObjectManagerV2::sInstance;

void ObjectManagerV2::StaticInit()
{
	sInstance.reset( new ObjectManagerV2() );

	//Is this safer ???
	//sInstance = std::make_unique<ObjectManagerV2>();
}

void ObjectManagerV2::Pause()
{
	m_bPause = true;
}

void ObjectManagerV2::PauseHUD()
{
	m_bPauseHUD = true;
}

void ObjectManagerV2::AddtoHUD(std::shared_ptr<GameObject> toAdd)
{
	m_AddListHUD.emplace_back(toAdd);
}

void ObjectManagerV2::AddNewObjectsHUD()
{
	if (!m_AddListHUD.empty())
	{
		m_HUDList.insert(m_HUDList.end(), m_AddListHUD.begin(), m_AddListHUD.end());
		//clear the add list
		m_AddListHUD.clear();
	}
}

void ObjectManagerV2::UpdateHUD(sf::Time sinceStart, sf::Time elapsed)
{
	//may need to pass game time incase some delays
	if (!m_bPauseHUD)
	{
	
		//add new objects from list
		//AddNewObjectsHUD();
		for (auto& obj : m_HUDList)
		{
			obj->Update(sinceStart, elapsed);
		}
	}
}

int ObjectManagerV2::DrawHUD(sf::RenderWindow & renderWindow, sf::RectangleShape viewRectangle)
{

	if (!m_bPauseHUD)
	{
		//add new objects from list
		AddNewObjectsHUD();

		for (auto& obj : m_HUDList)
		{
				renderWindow.draw(obj->sprite);
				renderWindow.draw(obj->text);
		}
		return m_HUDList.size();
	}

	return 0;
}

int ObjectManagerV2::DrawDebugHUD(sf::RenderWindow & renderWindow)
{
	if (!m_bPause)
	{
		//cull objects not visible to camera

		for (auto& obj : m_HUDList)
		{
			renderWindow.draw(obj->collisionRect);
		}
		return m_HUDList.size();
	}

	return 0;
}

void ObjectManagerV2::Add(std::shared_ptr<GameObject> toAdd)
{
	//std::cout << "adding game object to object manager with ID: " << toAdd->GetNetworkId() << std::endl;
	m_updateList.emplace_back(toAdd);
	toAdd->SetIndexInWorld(m_updateList.size() - 1);
	//m_updateList.emplace_back(toAdd);
}


void ObjectManagerV2::Remove(std::shared_ptr<GameObject> toRemove)
{
	/*int index = GetObjectIndex(toRemove);
	m_disposeList.emplace_back(index);*/

	int index = toRemove->GetIndexInWorld();

	int lastIndex = m_updateList.size() - 1;
	if (index != lastIndex)
	{
		m_updateList[index] = m_updateList[lastIndex];
		m_updateList[index]->SetIndexInWorld(index);
	}

	toRemove->SetIndexInWorld(-1);

	m_updateList.pop_back();
}

void ObjectManagerV2::RemoveFromVector(int index)
{
	if (index != -1)
	{
		int lastIndex = m_updateList.size() - 1;
		if (index <= lastIndex)
		{
			m_updateList[index] = m_updateList[lastIndex];
		}
		m_updateList.pop_back();
	}
}

int ObjectManagerV2::GetObjectIndex(std::shared_ptr<GameObject> toGet) const
{
	for (int i = 0, c = m_updateList.size(); i < c; ++i)
	{
		if (m_updateList[i] == toGet)
		{
			return i;
		}
	}

	return -1;
}

void ObjectManagerV2::DisposeRemoved()
{
	if (!m_disposeList.empty())
	{
		for (auto & index : m_disposeList)
		{
			RemoveFromVector(index);
		}
		//clear the disposed list
		m_disposeList.clear();
	}

}

void ObjectManagerV2::AddNewObjects()
{
	if (!m_AddList.empty())
	{
		m_updateList.insert(m_updateList.end(), m_AddList.begin(), m_AddList.end());
		//clear the add list
		m_AddList.clear();
	}
}

int ObjectManagerV2::setDrawList(sf::RectangleShape viewRectangle)
{
	m_drawList.clear();
	m_drawList.reserve(3);
	m_drawListZ0.clear();
	m_drawListZ1.clear();
	m_drawListZ2.clear();
	m_drawList.emplace_back(m_drawListZ0);
	m_drawList.emplace_back(m_drawListZ1);
	m_drawList.emplace_back(m_drawListZ2);
	int32_t count{ 0 };

	for (auto& obj : m_updateList)
	{
		if (obj->collisionRect.getGlobalBounds().intersects(viewRectangle.getGlobalBounds()))
		{
			m_drawList[obj->getZIndex()].emplace_back(obj);
			count++;
		}
	}

	return count;

}

int ObjectManagerV2::Update()
{
	////may need to pass game time incase some delays
	//if (!m_bPause)
	//{
	//	//dispose unused here
	//	DisposeRemoved();
	//	//add new objects from list
	//	AddNewObjects();

	//	for (auto& obj : m_updateList)
	//	{
	//		obj->Update();
	//	}
	//}

	for (int i = 0, c = m_updateList.size(); i < c; ++i)
	{
		GameObjectPtr go = m_updateList[i];


		if (!go->DoesWantToDie())
		{
			go->Update();
		}
		//you might suddenly want to die after your update, so check again
		if (go->DoesWantToDie())
		{
			Remove(go);
			go->HandleDying();
			--i;
			--c;
		}
	}

	return m_updateList.size();
}

int ObjectManagerV2::Update(sf::Time sinceStart, sf::Time elapsed)
{

	//may need to pass game time incase some delays
	if (!m_bPause)
	{
		//dispose unused here
		DisposeRemoved();
		//add new objects from list
		AddNewObjects();

		for (auto& obj : m_updateList)
		{
			obj->Update(sinceStart,elapsed);
		}
	}

	return m_updateList.size();
}


//this function will cull objects not seen by camera made by Lukas
int ObjectManagerV2::Draw(sf::RenderWindow& renderWindow, sf::RectangleShape viewRectangle)
{

	if (!m_bPause)
	{
		//cull objects not visible to camera
		setDrawList(viewRectangle);

		for (auto& list : m_drawList)
		{
			for (auto& obj : list)
			{
				renderWindow.draw(obj->sprite);
				renderWindow.draw(obj->text);
			}
		}
		return m_drawList[0].size() + m_drawList[1].size() + m_drawList[2].size();
	}

	return 0;
}


int ObjectManagerV2::Draw(sf::RenderWindow& renderWindow)
{

	if (!m_bPause)
	{
		
			for (auto& obj : m_updateList)
			{
				renderWindow.draw(obj->sprite);
				renderWindow.draw(obj->text);
			}

			return m_updateList.size();
	}

	return 0;
}



//for the bounding box visibility in game
int ObjectManagerV2::DrawDebug(sf::RenderWindow& renderWindow)
{
	if (!m_bPause)
	{
		//cull objects not visible to camera

		for (auto& obj : m_updateList)
		{
			renderWindow.draw(obj->collisionRect);
		}
		return m_updateList.size();
	}

	return 0;
}