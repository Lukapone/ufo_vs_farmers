#pragma once
class UFO;
class CowCounter;

class UFO_beam : public GameObject
{
private:
	//int32_t m_timeSinceStartBeam{0};
	std::shared_ptr<CowCounter> m_theCowCount;

public:
	float m_timeSinceStartBeam{ 0 };
	//std::shared_ptr<CowCounter> m_theCowCount;
	CLASS_IDENTIFICATION('BEAM', GameObject)

	enum EBeamReplicationState
	{
		EBMS_Position = 1 << 0,

		EBMS_AllState = EBMS_Position
	};

	static	GameObject*	StaticCreate() { return new UFO_beam(); }
	virtual uint32_t	GetAllStateMask()	const override { return EBMS_AllState; }
	UFO_beam();

	//OVERRIDEN VIRTUAL FUNCTIONS
	virtual void Update(sf::Time sinceStart, sf::Time elapsed) override;
	virtual ClassIdentification class_identification() override;
	virtual std::unique_ptr<GameObject> Clone() override;
	virtual std::shared_ptr<GameObject> SharedClone() override;


	//class functions
	void CenterBeamToUFO(UFO & toCenter);
	void checkAndApplyCollision(sf::Time sinceStart);
	void setCowCount(std::shared_ptr<CowCounter> toSet);


};

typedef std::shared_ptr< UFO_beam > UFO_beamPtr;