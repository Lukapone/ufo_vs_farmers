#pragma once

class SoundHolder
{
private:
	std::map<SoundID, std::unique_ptr<sf::SoundBuffer >> mSoundMap;
	std::map<SoundID, std::unique_ptr<sf::Sound>> mPlaySoundsMap;

public:
	static std::unique_ptr< SoundHolder >	sInstance;

	static void StaticInit();
	void loadSoundBuffers();

	void load(SoundID id, const std::string& filename);
	void play(SoundID id);
};