#include"UFO_FarmersShared.h"

void loadTextures();
//int loadGameObjectsIntoManager(ObjectManagerV2 & toLoad);
#define PLAY_GAME 0
int main()
{

	
	/*sf::SoundBuffer sndBffr;

	sndBffr.loadFromFile("../Resources/gunshot.wav");

	sf::Sound snd;
	snd.setBuffer(sndBffr);

	snd.play();*/
	
	//TextureHolder::sInstance = std::make_unique<TextureHolder>();

	//loadTextures();
	//std::shared_ptr<UFO>myUFO = std::make_shared<UFO>();

	//

	//ObjectManagerV2::StaticInit();

	//ObjectManagerV2::sInstance->Add(myUFO);
	//sf::RenderWindow window(sf::VideoMode(800, 600), "UFO vs Farmers");
	//while (window.isOpen())
	//{
	//
	//	int updated = ObjectManagerV2::sInstance->Update();
	//
	//	sf::Event event;
	//	while (window.pollEvent(event))
	//	{
	//		if (event.type == sf::Event::Closed)
	//			window.close();
	//	}

	//	ObjectManagerV2::sInstance->Draw(window);

	//	//need to set this for the counter
	//	window.setView(window.getDefaultView());

	//	//update the window
	//	window.display();
	//	//clear the screen
	//	window.clear();
	//}


	//Engine::sInstance = std::make_unique<Engine>();

	//Engine::sInstance->Run();

	//LOG("Shared version main", 0);


#if PLAY_GAME 

	//variables
	int drawn_objects{ 0 };

	sf::RenderWindow window(sf::VideoMode(800, 600), "UFO vs Farmers");
	//window.setFramerateLimit(60);

	ObjectManagerV2 objectManager;

	//initialize object manager for all game objects
	GameObject::objectManager = &objectManager;

	//int error = loadGameObjectsIntoManager(objectManager);
	//std::cout <<"Loading error: " << error << std::endl;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
															//LOADING TEXTURES AND CONVERTING TO SPRITES
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//path to get to the folder with images
	std::string resourcePath = { "../Resources/" };

	sf::Font font;
	if (!font.loadFromFile(resourcePath + "arial.ttf"))
	{
		return EXIT_FAILURE;
	}

	//// Create a text
	//sf::Text text("hello", font);
	//text.setCharacterSize(30);
	//text.setStyle(sf::Text::Bold);
	//text.setColor(sf::Color::Red);

	sf::Texture textureGras_bg;
	if (!textureGras_bg.loadFromFile(resourcePath + "gras_bg.jpg", sf::IntRect(0, 0, 1024, 768)))
	{
		return EXIT_FAILURE;
	}

	textureGras_bg.setSmooth(false);//smooth the edges of the texture
	textureGras_bg.setRepeated(true);//repeat the texture

									//UFO ufo1;
	std::shared_ptr<GameObject>myBG = std::make_shared<GameObject>();
	myBG->sprite.setTexture(textureGras_bg);
	myBG->sprite.setTextureRect(sf::IntRect(0, 0, 5120, 768));
	myBG->collisionRect.setSize(sf::Vector2f(4096, 768));
	objectManager.Add(myBG);


	sf::Texture textureCloud_bg;
	if (!textureCloud_bg.loadFromFile(resourcePath + "cloud.png"))
	{
		return EXIT_FAILURE;
	}

	textureCloud_bg.setSmooth(false);//smooth the edges of the texture
	std::shared_ptr<GameObject>myCloud = std::make_shared<GameObject>();
	myCloud->sprite.setTexture(textureCloud_bg);
	myCloud->sprite.setPosition(50, 20);
	myCloud->sprite.setScale(0.5, 0.3);

	myCloud->collisionRect.setSize(sf::Vector2f(600, 442));
	myCloud->collisionRect.setPosition(myCloud->sprite.getPosition());
	myCloud->collisionRect.setScale(myCloud->sprite.getScale());

	objectManager.Add(myCloud);

	//add clowds into game
	std::shared_ptr<GameObject>toAdd;
	int cloudCount = 8;
	for (int i = 0; i < cloudCount; i++)
	{
		//std::shared_ptr<CowObject>myCow2 = std::make_shared<CowObject>();
		//int generated = getRandomIntWithSeed(2 + i, 100, 4000);
		//std::cout << "generated : " << generated << std::endl;
		////myCow2->sprite.setTexture(textureCow);
		//myCow2->collisionRect.setPosition(generated, 510);
		//objectManager.Add(myCow2);
		toAdd = myCloud->SharedClone();
		toAdd->collisionRect.setPosition(getRandomIntWithSeed(2 + i, 100, 4000), getRandomIntWithSeed(2 + i, 0, 200));
		toAdd->sprite.setPosition(toAdd->collisionRect.getPosition());
		objectManager.Add(toAdd);
	}

	sf::Texture textureTree_bg;
	if (!textureTree_bg.loadFromFile(resourcePath + "tree.png"))
	{
		return EXIT_FAILURE;
	}

	textureTree_bg.setSmooth(false);//smooth the edges of the texture
	std::shared_ptr<GameObject>myTree = std::make_shared<GameObject>();
	myTree->sprite.setTexture(textureTree_bg);
	myTree->sprite.setPosition(50,330);
	myTree->sprite.setScale(0.5, 0.3);

	myTree->collisionRect.setSize(sf::Vector2f(600, 442));
	myTree->collisionRect.setPosition(myTree->sprite.getPosition());
	myTree->collisionRect.setScale(myTree->sprite.getScale());

	objectManager.Add(myTree);

	//add clowds into game
	std::shared_ptr<GameObject>toAddTree;
	int treeCount = 8;
	for (int i = 0; i < treeCount; i++)
	{
		//std::shared_ptr<CowObject>myCow2 = std::make_shared<CowObject>();
		//int generated = getRandomIntWithSeed(2 + i, 100, 4000);
		//std::cout << "generated : " << generated << std::endl;
		////myCow2->sprite.setTexture(textureCow);
		//myCow2->collisionRect.setPosition(generated, 510);
		//objectManager.Add(myCow2);
		toAddTree = myTree->SharedClone();
		toAddTree->collisionRect.setPosition(getRandomIntWithSeed(3 + i, 100, 4000), 330);
		toAddTree->sprite.setPosition(toAddTree->collisionRect.getPosition());
		objectManager.Add(toAddTree);
	}

	sf::Texture textureBarn_bg;
	if (!textureBarn_bg.loadFromFile(resourcePath + "red_barn.png"))
	{
		return EXIT_FAILURE;
	}

	textureBarn_bg.setSmooth(false);//smooth the edges of the texture
	std::shared_ptr<GameObject>myBarn = std::make_shared<GameObject>();
	myBarn->sprite.setTexture(textureBarn_bg);
	myBarn->sprite.setPosition(4000, 0);
	myBarn->sprite.setScale(1, 1);

	myBarn->collisionRect.setSize(sf::Vector2f(600, 588));
	myBarn->collisionRect.setPosition(myBarn->sprite.getPosition());
	myBarn->collisionRect.setScale(myBarn->sprite.getScale());

	objectManager.Add(myBarn);


	//sf::Texture textureGras_bg;
	//if (!textureGras_bg.loadFromFile(resourcePath + "gras_bg.jpg", sf::IntRect(0, 0, 1024, 768)))
	//{
	//	return EXIT_FAILURE;
	//}

	//textureGras_bg.setSmooth(false);//smooth the edges of the texture
	//textureGras_bg.setRepeated(true);//repeat the texture

	//								 //UFO ufo1;
	//std::shared_ptr<GameObject>myBG = std::make_shared<GameObject>();
	//myBG->sprite.setTexture(textureGras_bg);
	//myBG->sprite.setTextureRect(sf::IntRect(0, 0, 4096, 768));
	//myBG->collisionRect.setSize(sf::Vector2f(4096, 768));
	//objectManager.Add(myBG);


	sf::Texture textureUFO_beam;
	if (!textureUFO_beam.loadFromFile(resourcePath + "blue_beam.png", sf::IntRect(0, 0, 420, 330)))
	{
		return EXIT_FAILURE;
	}
	textureUFO_beam.setSmooth(true);//smooth the edges of the texture

							   //UFO ufo1;
	std::shared_ptr<UFO_beam>myBeam = std::make_shared<UFO_beam>();
	myBeam->sprite.setTexture(textureUFO_beam);
	//objectManager.Add(myBeam);


	sf::Texture textureUFO;
	if (!textureUFO.loadFromFile(resourcePath + "ufos.png", sf::IntRect(0, 0, 415, 330)))
	{
		return EXIT_FAILURE;
	}
	textureUFO.setSmooth(true);//smooth the edges of the texture

							   //UFO ufo1;
	std::shared_ptr<UFO>myUFO = std::make_shared<UFO>();
	myUFO->sprite.setTexture(textureUFO);
	myUFO->text.setFont(font);
	objectManager.Add(myUFO);
	myUFO->SetBeam(myBeam);



	sf::Texture textureCow;
	if (!textureCow.loadFromFile(resourcePath + "cow_black_white.png"))
	{
		return EXIT_FAILURE;
	}
	textureUFO.setSmooth(true);//smooth the edges of the texture

							   //cow
	std::shared_ptr<CowObject>myCow = std::make_shared<CowObject>();
	myCow->sprite.setTexture(textureCow);
	//objectManager.Add(myCow);


	//std::shared_ptr<CowObject>myCow3 = std::make_shared<CowObject>();
	//myCow3->sprite.setTexture(textureCow);
	//myCow3->collisionRect.setPosition(300, 100);
	//objectManager.Add(myCow3);


	//add cows into game
	std::shared_ptr<GameObject>toAddCow;
	int cowCount = 17;
	for (int i = 0; i < cowCount; i++)
	{
		//std::shared_ptr<CowObject>myCow2 = std::make_shared<CowObject>();
		//int generated = getRandomIntWithSeed(2 + i, 100, 4000);
		//std::cout << "generated : " << generated << std::endl;
		////myCow2->sprite.setTexture(textureCow);
		//myCow2->collisionRect.setPosition(generated, 510);
		//objectManager.Add(myCow2);
		toAddCow = myCow->SharedClone();
		toAddCow->collisionRect.setPosition(getRandomIntWithSeed(2 + i, 100, 4000), 510);
		objectManager.Add(toAddCow);
	}
	
	sf::Texture textureFarmer;
	if (!textureFarmer.loadFromFile(resourcePath + "cowboy.png"))
	{
		return EXIT_FAILURE;
	}
	textureFarmer.setSmooth(true);

	//Farmer 1
	std::shared_ptr<FarmerObject>myFarmer = std::make_shared<FarmerObject>();
	myFarmer->sprite.setTexture(textureFarmer);
	objectManager.Add(myFarmer);

	//add gun
	sf::Texture textureGun;
	if (!textureGun.loadFromFile(resourcePath + "gun.png"))
	{
		return EXIT_FAILURE;
	}
	textureGun.setSmooth(true);

	//Gun1
	std::shared_ptr<GunObject>myGun = std::make_shared<GunObject>();
	myGun->sprite.setTexture(textureGun);
	myGun->sprite.setPosition(myFarmer->sprite.getPosition());

	objectManager.Add(myGun);
	myFarmer->SetGun(myGun);

	//Black middle line


	//add bullet
	sf::Texture textureBullet;
	if (!textureBullet.loadFromFile(resourcePath + "bullet.png"))
	{
		return EXIT_FAILURE;
	}

	//Gun1
	std::shared_ptr<BulletObject>myBullet = std::make_shared<BulletObject>();
	myBullet->sprite.setTexture(textureBullet);
	myGun->SetBullet(myBullet);

	//box for cow counter
	int32_t lineWidth2 = 250;
	sf::RectangleShape blackLine2(sf::Vector2f(lineWidth2, 50));
	blackLine2.setPosition((window.getSize().x / 2) - lineWidth2 / 2, 0);


	//abductedCows
	std::shared_ptr<CowCounter>myCowCounter = std::make_shared<CowCounter>();
	myCowCounter->text.setFont(font);
	myCowCounter->text.setPosition((window.getSize().x / 2)-100, 0);
	myCowCounter->setCount(cowCount);
	myCowCounter->text.setString("Cow counter: " + std::to_string(cowCount));
	myBeam->setCowCount(myCowCounter);

	objectManager.AddtoHUD(myCowCounter);


	//UFO victory screen
	std::shared_ptr<GameObject>victoryUFOscreen = std::make_shared<GameObject>();
	victoryUFOscreen->setZIndex(2);
	victoryUFOscreen->text.setFont(font);
	victoryUFOscreen->text.setCharacterSize(120);
	victoryUFOscreen->text.setColor(sf::Color::Blue);
	victoryUFOscreen->text.setPosition((window.getSize().x / 2) - 270, 150);
	victoryUFOscreen->text.setString("UFO's won !");
	myCowCounter->setVictoryScreen(victoryUFOscreen);



	//FARMERS victory screen
	std::shared_ptr<GameObject>victoryFarmerScreen = std::make_shared<GameObject>();
	victoryFarmerScreen->setZIndex(2);
	victoryFarmerScreen->text.setFont(font);
	victoryFarmerScreen->text.setCharacterSize(80);
	victoryFarmerScreen->text.setColor(sf::Color::Blue);
	victoryFarmerScreen->text.setPosition((window.getSize().x / 2) - 270, 150);
	victoryFarmerScreen->text.setString("FARMERS's won !");
	myBullet->SetFarmersVictoryScreen(victoryFarmerScreen);

	//objectManager.AddtoHUD(victoryUFOscreen);

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//SETTING CAMERA2D VIEW MADE BY LUKAS
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	sf::Vector2f viewPosition(0, 0);
	sf::View view_payer1;
	view_payer1.reset(sf::FloatRect(0, 0, window.getSize().x, window.getSize().y));
	view_payer1.setViewport(sf::FloatRect(0, 0, 1.0f, 1.0f));
	sf::RectangleShape viewRectangle_player1(sf::Vector2f(window.getSize().x -20, window.getSize().y -20));
	viewRectangle_player1.setOrigin(window.getSize().x / 2, window.getSize().y / 2);

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//GAME LOOP MADE BY LUKAS
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//CREATING CLOCK TO UPDATE TIME IN GAME AND ADD DELAYS MADE BY LUKAS
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	sf::Clock clock;
	sf::Time elapsed;
	double t = 0.0;  //http://gafferongames.com/game-physics/fix-your-timestep/ for decoupling physics and update
	constexpr double dt = 0.01;
	constexpr double targetFPS = 60;
	constexpr double targetFrameTime = 1 / targetFPS;
	double newTime = 0.0;
	double frameTime = 0.0;
	double currentTime = clock.getElapsedTime().asSeconds();
	double accumulator = 0.0;
	int FPS{ 0 };




	while (window.isOpen())
	{
		newTime = clock.getElapsedTime().asSeconds();
		frameTime = newTime - currentTime;

		//std::cout << "Frame Time: " << frameTime << std::endl;
		if (frameTime > targetFrameTime)
		{
			frameTime = targetFrameTime;
		}

		//std::cout << "Adjusted Frame Time: " << frameTime << std::endl;
		currentTime = newTime;
		accumulator += frameTime;

		while (accumulator >= dt)
		{
			//here will be the update as many time as the physics needs
			sf::Time physics_update_step = sf::seconds(dt);
			sf::Time time_fromStart = sf::seconds(t);
			int updated = objectManager.Update(time_fromStart, physics_update_step);
			//std::cout << "updated " << updated << std::endl;
			objectManager.UpdateHUD(time_fromStart, physics_update_step);
			t += dt;
			accumulator -= dt;
			FPS++;
		}

		//std::cout <<"Acumulator left over: " << accumulator << std::endl;

		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		//update the viewport
		if (myUFO->collisionRect.getPosition().x > window.getSize().x / 2)
		{
			viewPosition.x = myUFO->collisionRect.getPosition().x;
		}
		else
		{
			viewPosition.x = window.getSize().x / 2;
		}

		if (myUFO->collisionRect.getPosition().y > window.getSize().y / 2)
		{
			viewPosition.y = myUFO->collisionRect.getPosition().y;
		}
		else
		{
			//viewPosition.y = myUFO->collisionRect.getPosition().y;
			viewPosition.y = window.getSize().y / 2;
		}
		view_payer1.setCenter(viewPosition);
		viewRectangle_player1.setPosition(viewPosition);
		viewRectangle_player1.setFillColor(sf::Color::Red);
		window.setView(view_payer1);

		drawn_objects = objectManager.Draw(window, viewRectangle_player1);
		std::cout << "Drawn objects screen 1: " << drawn_objects << std::endl;

		//need to set this for the counter
		window.setView(window.getDefaultView());
		window.draw(blackLine2);
		drawn_objects = objectManager.DrawHUD(window, viewRectangle_player1);
		//drawn_objects = objectManager.DrawDebugHUD(window);
	
		//update the window
		window.display();
		//clear the screen
		window.clear();
	}

#endif

	system("PAUSE");

	return 0;
}

void loadTextures()
{
	//path to get to the folder with images
	std::string resourcePath = { "../Resources/" };
	TextureHolder::sInstance->load(TextureID::UFO, resourcePath + "ufos.png", sf::IntRect(0, 0, 415, 330));

}
