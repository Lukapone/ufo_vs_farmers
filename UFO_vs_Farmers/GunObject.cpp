#include"UFO_FarmersShared.h"

//made by Paddy

GunObject::GunObject()
{
	sprite.setTexture(TextureHolder::sInstance->get(TextureID::Gun));
	sprite.setPosition(sf::Vector2f(300, 100));
	sprite.setOrigin(0, 30);
	m_ZIndex = 1;
	//sprite.rotate(180);
	collisionRect.setSize(sf::Vector2f(26, 31));
}



uint32_t GunObject::Write(OutputMemoryBitStream & inOutputStream, uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	if (inDirtyState & EGUN_PositionChange)
	{
		inOutputStream.Write((bool)true);
		uint32_t posX = sprite.getPosition().x;
		uint32_t posY = sprite.getPosition().y;
		inOutputStream.Write(posX, 12);
		inOutputStream.Write(posY, 12);

		writtenState |= EGUN_PositionChange;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}


	if (inDirtyState & EGUN_Rotation)
	{
		inOutputStream.Write((bool)true);
		uint32_t rotationInt = sprite.getRotation();
		inOutputStream.Write(rotationInt, 9);

		writtenState |= EGUN_Rotation;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	//std::cout << "after using WRITE the writtenstate is : " << writtenState << std::endl;
	return writtenState;
}

void GunObject::Update(sf::Time sinceStart, sf::Time elapsed)
{
	//sprite.setPosition(100, 100);
	HandleInput(sinceStart, elapsed);
	collisionRect.setPosition(sprite.getPosition());

	//std::cout << "Here" << std::endl;
	//m_shotLock = false;
}

void GunObject::Update()
{
	//std::cout << "base update of gun" << std::endl;
}

ClassIdentification GunObject::class_identification()
{
	return ClassIdentification::GunObject;
}

std::unique_ptr<GameObject> GunObject::Clone()
{
	return std::unique_ptr<GameObject>();
}

std::shared_ptr<GameObject> GunObject::SharedClone()
{
	return std::shared_ptr<GameObject>();
}

//rotates the gun sprite/fires the bullets
void GunObject::HandleInput(sf::Time sinceStart, sf::Time elapsed)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		m_rotationInDegrees++;
		m_rotationInDegrees %= 360;
		sprite.rotate(1);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		m_rotationInDegrees--;
		m_rotationInDegrees %= 360;
		sprite.rotate(-1);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
	{

		m_timeSinceLastShot += elapsed.asMilliseconds();
		if (m_timeSinceLastShot > 300)
		{
			Shoot(sinceStart);
			m_timeSinceLastShot = 0;
		}
		////m_Bullet->setPosition(*this);
		//if (!m_shotLock)
		//{
		//	m_shotLock = true;
		//	if (sinceStart.asSeconds() - m_timePressed < 1)
		//	{
		//		
		//	}
		//	else
		//	{
		//		m_isMoving = false;
		//		destroy();
		//	}
		//	
		//	
		//}
	}
	else
	{
		m_timeSinceLastShot += elapsed.asMilliseconds();
	}

	//if (sf::Keyboard::isKeyPressed(sf::Keyboard::RControl))
	//{
	//	m_shotLock = false;
	//}

}


//made by Lukas where the bullet is setup by the gun and moving in the direction of rotation
void GunObject::Shoot(sf::Time sinceStart)
{
	//std::cout << "firing" << std::endl;

	std::shared_ptr<GameObject>firedBullet = m_Bullet->SharedClone();
	
	BulletObject & objBullet = dynamic_cast<BulletObject&>(*firedBullet);
	objBullet.setPosition(sf::Vector2f(sprite.getPosition().x,sprite.getPosition().y));
	objBullet.setMovin(true);
	objBullet.setTimePressed(sinceStart);
	objBullet.setRotation(m_rotationInDegrees);
	objBullet.sprite.setRotation(m_rotationInDegrees);
	objBullet.SetSelfPointer(firedBullet);
	GameObject::objectManager->Add(firedBullet);
}
