
class InputState
{
public:

	/*InputState() :
	mDesiredRightAmount( 0 ),
	mDesiredLeftAmount( 0 ),
	mDesiredUpAmount( 0 ),
	mDesiredDownAmount( 0 ),
	mIsShooting( false )
	{}*/

	InputState();

	/*float GetDesiredHorizontalDelta()	const { return mDesiredRightAmount - mDesiredLeftAmount; }
	float GetDesiredVerticalDelta()		const { return mDesiredDownAmount - mDesiredUpAmount; }
	bool  IsShooting()					const { return mIsShooting; }*/
	bool is_W_pressed() const { return mIs_W_pressed; }
	bool is_S_pressed() const { return mIs_S_pressed; }
	bool is_A_pressed() const { return mIs_A_pressed; }
	bool is_D_pressed() const { return mIs_D_pressed; }
	bool is_C_pressed() const { return mIs_C_pressed; }

	bool Write( OutputMemoryBitStream& inOutputStream ) const;
	bool Read( InputMemoryBitStream& inInputStream );

private:
	friend class InputManager;

	//float	mDesiredRightAmount, mDesiredLeftAmount;
	//float	mDesiredUpAmount, mDesiredDownAmount;
	bool mIs_W_pressed{ false };
	bool mIs_S_pressed{ false };
	bool mIs_A_pressed{ false };
	bool mIs_D_pressed{ false };
	bool mIs_C_pressed{ false };
};