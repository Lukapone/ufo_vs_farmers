#pragma once
#include"UFO_beam.h"
#include"PhysicsIntegration.h"
class InputState;

class UFO : public GameObject
{
private:
	uint32_t			mPlayerId;
	

protected:
	
	ObjectState m_objState;

	float m_movementSpeed{ 1 };
	int32_t m_health{ 30 };

	bool m_canBeHit{ true };
	int32_t m_timeSinceLastHit{ 0 };

	bool m_previousBeamKeyState{ false };
	bool m_bBeaming{ false };
	bool m_bBeamActive{ false };

	bool m_bAcceleratingUP{ false };
	bool m_bAcceleratingDown{ false };
	bool m_bAcceleratingLeft{ false };
	bool m_bAcceleratingRight{ false };
	bool m_bRotatingLeft{ false };
	bool m_bRotatingRight{ false };
	bool m_velocityLock_x{ false };
	bool m_is_positive_x{ false };
	bool m_velocityLock_y{ false };
	bool m_is_positive_y{ false };

	float m_timePressed{0};
	float m_maxVelocity{ 0 };
	float m_rotatedLeftAngle{ 0 };
	float m_rotatedRightAngle{ 0 };
	float m_velocity{ 0 };
	float m_accelerationSides{ 0 };
	float m_accelerationUPDown{ 0 };
public:
	std::string m_PlayerName{ "noPlayerUFOName" };
	CLASS_IDENTIFICATION('CUFO', GameObject)

	enum EUFOReplicationState
	{
		ECRS_PositionChange = 1 << 0,
		ECRS_PlayerId = 1 << 1,
		ECRS_Health = 1 << 2,
		ECRS_Rotation = 1 << 3,

		ECRS_AllState = ECRS_PositionChange | ECRS_PlayerId | ECRS_Health | ECRS_Rotation
	};

	static	GameObject*	StaticCreate() { return new UFO(); }

	virtual uint32_t GetAllStateMask()	const override { return ECRS_AllState; }


	UFO();

	//OVERRIDEN VIRTUAL FUNCTIONS
	virtual void Update() override;
	virtual void Update(sf::Time sinceStart, sf::Time elapsed) override;

	virtual ClassIdentification class_identification() override;
	virtual std::unique_ptr<GameObject> Clone() override;
	virtual std::shared_ptr<GameObject> SharedClone() override;

	//class functions
	//void HandleInput(sf::Time sinceStart, sf::Time elapsed);
	/*void UpdateBeam();
	void AddAndUpdateBeam();*/
	void checkAndApplyCollision(sf::Vector2f oldPos);
	void takeDamage(int32_t amount);

	void makeVulnerableAfterTime(sf::Time elapsed);

	//inline void SetBeam(std::shared_ptr<UFO_beam> & toSet) { m_UFO_beam = toSet; }
	bool isDeath() const;

	







	//void ProcessInput(float inDeltaTime, const InputState & inInputState);
	void SimulateMovement(float inDeltaTime);


	virtual uint32_t Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;

	void		SetPlayerId(uint32_t inPlayerId) { mPlayerId = inPlayerId; }
	uint32_t	GetPlayerId()						const { return mPlayerId; }

};


typedef std::shared_ptr<UFO> UFOPtr;




