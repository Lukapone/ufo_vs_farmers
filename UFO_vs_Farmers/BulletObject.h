#pragma once
#include "GlobalVariables.h"

class BulletObject : public GameObject
{
private:
protected:
	float m_movementSpeed{ GB_BULLET_MOVEMENT_SPEED };
	int m_rotationInDegrees{ GB_BULLET_ROTATION_IN_DEGREES };
	int m_timeSinceFired{ GB_BULLET_TIME_SINCE_FIRED };
	int m_timePressed{ GB_BULLET_TIME_PRESSED };
	bool m_isMoving{ GB_BULLET_IS_MOVING };
	std::shared_ptr<GameObject> m_selfPointer;
	std::shared_ptr<GameObject> m_FarmersVictoryScreen;
public:
	BulletObject();

	CLASS_IDENTIFICATION('BULL', GameObject)

	enum EBULReplicationState
	{
		EBUL_PositionChange = 1 << 0,
		EBUL_Rotation = 1 << 1,

		EBUL_AllState = EBUL_PositionChange | EBUL_Rotation
	};

	static	GameObject*	StaticCreate() { return new BulletObject(); }

	virtual uint32_t GetAllStateMask()	const override { return EBUL_AllState; }

	virtual uint32_t Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;

	//OVERRIDEN VIRTUAL FUNCTIONS
	virtual void Update(sf::Time sinceStart, sf::Time elapsed) override;
	virtual void Update() override;
	virtual ClassIdentification class_identification() override;
	virtual std::unique_ptr<GameObject> Clone() override;
	virtual std::shared_ptr<GameObject> SharedClone() override;


	void HandleInput(sf::Time sinceStart, sf::Time elapsed);
	void move();

	void setPosition(const sf::Vector2f & position);
	void destroy();
	void setMovin(bool toSet);
	void setTimePressed(sf::Time pressed);
	void setRotation(int rotation);
	inline void SetSelfPointer(std::shared_ptr<GameObject> & toSet) { m_selfPointer = toSet; }
	void checkAndApplyCollision(sf::Time sinceStart);
	inline void SetFarmersVictoryScreen(std::shared_ptr<GameObject> & toSet) { m_FarmersVictoryScreen = toSet; }
	
};
