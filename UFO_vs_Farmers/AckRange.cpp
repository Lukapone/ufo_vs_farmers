#include"UFO_FarmersShared.h"

void AckRange::Write( OutputMemoryBitStream& inOutputStream ) const
{
	inOutputStream.Write( mStart );
	//std::cout << "writing acks start: " << mStart << std::endl;
	//std::cout << "4.write start sequence number: 16bits   = "<< inOutputStream.GetBitLength() << std::endl;
	bool hasCount = mCount > 1;
	inOutputStream.Write( hasCount );
	//std::cout << "5.Write has count Boolean: 1bit   = " << inOutputStream.GetBitLength() << std::endl;
	if( hasCount )
	{
		//most you can ack is 255...
		uint32_t countMinusOne = mCount - 1;
		uint8_t countToAck = countMinusOne > 255 ? 255 : static_cast< uint8_t >( countMinusOne );
		inOutputStream.Write( countToAck );
		//std::cout << "6.write count to acks 8bits   = " <<  inOutputStream.GetBitLength() << std::endl;
	}

}

void AckRange::Read( InputMemoryBitStream& inInputStream )
{
	inInputStream.Read( mStart );
	//std::cout << "reding ackrange mStart" << mStart << std::endl;
	bool hasCount;
	inInputStream.Read( hasCount );
	//std::cout << "reding ackrange hasCount" << hasCount << std::endl;
	if( hasCount )
	{
		uint8_t countMinusOne;
		inInputStream.Read( countMinusOne );
		//std::cout << "reding countMinus1" << countMinusOne << std::endl;
		mCount = countMinusOne + 1;
	}
	else
	{
		//default!
		mCount = 1;
	}
}