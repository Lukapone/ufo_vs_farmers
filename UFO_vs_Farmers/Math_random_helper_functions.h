#pragma once

int getRandomIntWithSeed(int seed, int min, int max);

int getRandomMt19937(int min, int max);

int getRandomCryptoSecure(int min, int max);