#include"UFO_FarmersShared.h"

std::unique_ptr< TextureHolder >	TextureHolder::sInstance;


void TextureHolder::StaticInit()
{
	//sInstance.reset(new TextureHolder()); // calls deleter for the old one
	//reason why to use make_unique over reset
	sInstance = std::make_unique<TextureHolder>();//http://stackoverflow.com/questions/16061407/should-i-assign-or-reset-a-unique-ptr
}

void TextureHolder::loadTextures()
{
	//path to get to the folder with images
	std::string resourcePath = { "../Resources/" };
	TextureHolder::sInstance->load(TextureID::UFO, resourcePath + "ufos.png", sf::IntRect(0, 0, 415, 330));
	TextureHolder::sInstance->load(TextureID::Beam, resourcePath + "blue_beam.png", sf::IntRect(0, 0, 128, 256));
	TextureHolder::sInstance->load(TextureID::Cow, resourcePath + "cow_black_white.png");
	//TextureHolder::sInstance->load(TextureID::Farmer, resourcePath + "cowboy.png", sf::IntRect(0, 0, 35, 46));
	TextureHolder::sInstance->load(TextureID::Farmer, resourcePath + "cowboy.png");
	TextureHolder::sInstance->load(TextureID::Background, resourcePath + "gras_bg.jpg", sf::IntRect(0, 0, 1024, 768));
	TextureHolder::sInstance->load(TextureID::Tree, resourcePath + "tree.png");
	TextureHolder::sInstance->load(TextureID::Barn, resourcePath + "red_barn.png");
	TextureHolder::sInstance->load(TextureID::Gun, resourcePath + "gun.png");
	TextureHolder::sInstance->load(TextureID::Bullet, resourcePath + "bullet.png");
	TextureHolder::sInstance->load(TextureID::Cloud, resourcePath + "cloud.png");
}

void TextureHolder::load(TextureID id, const std::string & filename)
{
	std::unique_ptr<sf::Texture> texture(std::make_unique<sf::Texture>());
	if (!texture->loadFromFile(filename))
		throw std::runtime_error("TextureHolder::load - Failed to load " + filename);

	auto inserted = mTextureMap.insert(std::make_pair(id, std::move(texture)));
	assert(inserted.second);
}

void TextureHolder::load(TextureID id, const std::string & filename, sf::IntRect inRect)
{
	std::unique_ptr<sf::Texture> texture(std::make_unique<sf::Texture>());
	if (!texture->loadFromFile(filename, inRect))
		throw std::runtime_error("TextureHolder::load - Failed to load " + filename);

	auto inserted = mTextureMap.insert(std::make_pair(id, std::move(texture)));
	assert(inserted.second);
}

sf::Texture & TextureHolder::get(TextureID id)
{
	auto found = mTextureMap.find(id);
	return *found->second;
}

const sf::Texture & TextureHolder::get(TextureID id) const
{
	const auto found = mTextureMap.find(id);
	return *found->second;
}
