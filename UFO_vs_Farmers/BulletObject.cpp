#include"UFO_FarmersShared.h"


BulletObject::BulletObject()
{

	sprite.setTexture(TextureHolder::sInstance->get(TextureID::Bullet));


	m_ZIndex = 2;
	collisionRect.setSize(sf::Vector2f(326, 326));
	collisionRect.setFillColor(sf::Color::Red);
	collisionRect.setScale(0.05, 0.05);
	collisionRect.setPosition(0, 0);//this is in centre of screen needs to be adapted to diferent resolutions
	sprite.setScale(collisionRect.getScale());
	sprite.setPosition(collisionRect.getPosition());
}

uint32_t BulletObject::Write(OutputMemoryBitStream & inOutputStream, uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	if (inDirtyState & EBUL_PositionChange)
	{
		inOutputStream.Write((bool)true);
		uint32_t posX = sprite.getPosition().x;
		uint32_t posY = sprite.getPosition().y;
		inOutputStream.Write(posX, 12);
		inOutputStream.Write(posY, 12);

		writtenState |= EBUL_PositionChange;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}


	if (inDirtyState & EBUL_Rotation)
	{
		inOutputStream.Write((bool)true);
		uint32_t rotationInt = sprite.getRotation();
		inOutputStream.Write(rotationInt, 9);

		writtenState |= EBUL_Rotation;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	//std::cout << "after using WRITE the writtenstate is : " << writtenState << std::endl;
	return writtenState;
}

void BulletObject::Update(sf::Time sinceStart, sf::Time elapsed)
{
	//HandleInput(sinceStart, elapsed);
	sprite.setPosition(collisionRect.getPosition());
	checkAndApplyCollision(elapsed);

	if (m_isMoving)
	{
		if (sinceStart.asSeconds() - m_timePressed < 2)
		{
			move();
		}
		else
		{
			m_isMoving = false;
			destroy();
		}
	}
}

void BulletObject::Update()
{
	std::cout << "base update of bullet" << std::endl;
}

ClassIdentification BulletObject::class_identification()
{
	return ClassIdentification::BulletObject;
}

std::unique_ptr<GameObject> BulletObject::Clone()
{
	return std::make_unique<BulletObject>(*this);
}

std::shared_ptr<GameObject> BulletObject::SharedClone()
{
	return std::make_shared<BulletObject>(*this);
}

void BulletObject::HandleInput(sf::Time sinceStart, sf::Time elapsed)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		m_rotationInDegrees++;
		m_rotationInDegrees %= 360;
		std::cout << "Rotation in degrees: " << m_rotationInDegrees << std::endl;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		m_rotationInDegrees--;
		m_rotationInDegrees %= 360;
		std::cout << "Rotation in degrees: " << m_rotationInDegrees << std::endl;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
	{
		m_isMoving = true;
		m_timePressed = sinceStart.asSeconds();
	}
}

//moves the bullet in a diagonal path using the roatation of the gun - made by Paddy
void BulletObject::move()
{
	sprite.move(sin(m_rotationInDegrees*3.14159265 / 180)*m_movementSpeed, cos(m_rotationInDegrees*3.14159265 / 180) * -m_movementSpeed);
	collisionRect.move(sin(m_rotationInDegrees*3.14159265 / 180)*m_movementSpeed, cos(m_rotationInDegrees*3.14159265 / 180) * -m_movementSpeed);

}

void BulletObject::setPosition(const sf::Vector2f & position)
{
	sprite.setPosition(position);
	collisionRect.setPosition(position);
}

void BulletObject::destroy()
{
	GameObject::objectManager->Remove(m_selfPointer);
}

void BulletObject::setMovin(bool toSet)
{
	m_isMoving = toSet;
}

void BulletObject::setTimePressed(sf::Time pressed)
{
	m_timePressed = pressed.asSeconds();
}

void BulletObject::setRotation(int rotation)
{
	m_rotationInDegrees = rotation;
}


//made by Lukas where bullet will do damage to UFO if it collides with it
void BulletObject::checkAndApplyCollision(sf::Time elapsed)
{
	for (auto & gameObj : GameObject::objectManager->getUpdateList())
	{
		if (gameObj->class_identification() == ClassIdentification::UFO)
		{
			if (gameObj->collisionRect.getGlobalBounds().intersects(this->collisionRect.getGlobalBounds()))
			{
					//std::cout << "damage " << std::endl;
					UFO & objUFO = dynamic_cast<UFO&>(*gameObj);
					objUFO.takeDamage(1);
					GameObject::objectManager->Remove(m_selfPointer);

					if (objUFO.isDeath())
					{
						GameObject::objectManager->Remove(gameObj);
						GameObject::objectManager->AddtoHUD(m_FarmersVictoryScreen);
						GameObject::objectManager->Pause();
						return;
					}
			}
		}
	}
	
}
