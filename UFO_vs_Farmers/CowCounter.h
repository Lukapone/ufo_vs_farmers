#pragma once


class CowCounter : public GameObject
{
private:

	std::shared_ptr<GameObject> m_victoryScreen;
protected:

	int32_t m_cowCount{ 0 };
public:
	CowCounter();

	//OVERRIDEN VIRTUAL FUNCTIONS
	virtual void Update(sf::Time sinceStart, sf::Time elapsed) override;
	void Update();
	virtual ClassIdentification class_identification() override;
	virtual std::unique_ptr<GameObject> Clone() override;
	virtual std::shared_ptr<GameObject> SharedClone() override;

	void setCount(int32_t toSet);
	int32_t getCount() const;
	void setVictoryScreen(std::shared_ptr<GameObject> victoryScreen);

	void abductCow();

	CLASS_IDENTIFICATION('CCNT', GameObject)

	enum ECountReplicationState
	{
		ECNT_Count = 1 << 0,

		ECNT_AllState = ECNT_Count
	};

	static	GameObject*	StaticCreate() { return new CowObject(); }

	virtual uint32_t	GetAllStateMask()	const override { return ECNT_AllState; }


};