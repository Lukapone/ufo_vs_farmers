#pragma once
#include "GlobalVariables.h"

class GunObject;

class FarmerObject : public GameObject
{

	protected:
		float m_move_speed { GB_FARMER_MOVE_SPEED };
		int m_counter { GB_FARMER_COUNTER };
		int m_timeSinceLastFrame { GB_FARMER_TIME_SINCE_LAST_FRAME };
		bool m_isLeft { GB_FARMER_IS_LEFT };
		bool m_disableMoveRight{ GB_FARMER_DISABLE_MOVE_RIGHT };
		bool m_disableMoveLeft{ GB_FARMER_DISBALE_MOVE_LEFT };
		
		uint32_t mPlayerId;
	public:
		std::shared_ptr<GunObject> m_farmersGun;
		CLASS_IDENTIFICATION('FRMR', GameObject)

		enum FRMRReplicationState
		{
			FRMR_PositionChange = 1 << 0,
			FRMR_PlayerId = 1 << 1,
			FRMR_SpriteChanged = 1 << 2,

			FRMR_AllState = FRMR_PositionChange | FRMR_PlayerId | FRMR_SpriteChanged
		};

		static	GameObject*	StaticCreate() { return new FarmerObject(); }

		void		SetPlayerId(uint32_t inPlayerId) { mPlayerId = inPlayerId; }
		uint32_t	GetPlayerId()						const { return mPlayerId; }





		FarmerObject();

		//OVERRIDEN VIRTUAL FUNCTIONS
		virtual void Update(sf::Time sinceStart, sf::Time elapsed) override;
		virtual void Update() override;
		virtual ClassIdentification class_identification() override;
		virtual std::unique_ptr<GameObject> Clone() override;
		virtual std::shared_ptr<GameObject> SharedClone() override;

		//class functions
		void HandleInput(sf::Time sinceStart, sf::Time elapsed); 

		void checkAndApplyCollision();

		inline void SetGun(std::shared_ptr<GunObject> & toSet) { m_farmersGun = toSet; }

		virtual uint32_t Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;
};

typedef std::shared_ptr<FarmerObject> FarmerPtr;