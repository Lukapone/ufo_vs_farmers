#pragma once
#include "GlobalVariables.h"

class CowObject : public GameObject
{
private:
	float m_movementSpeed{ GB_COW_MOVEMENT_SPEED };
	int32_t m_health{ GB_COW_HEALTH };


public:
	CowObject();

	//OVERRIDEN VIRTUAL FUNCTIONS
	virtual void Update() override;
	virtual void Update(sf::Time sinceStart, sf::Time elapsed) override;
	virtual ClassIdentification class_identification() override;
	virtual std::unique_ptr<GameObject> Clone() override;
	virtual std::shared_ptr<GameObject> SharedClone() override;

	//class functions
	void HandleInput();
	void takeDamege(int32_t amount);
	void checkAndApplyCollision(sf::Time sinceStart);

	//geters seters
	inline int32_t getHealth() const;
	bool isDeath() const;

	CLASS_IDENTIFICATION('CCOW', GameObject)

	enum ECowReplicationState
	{
		ECOW_Position = 1 << 0,
	
		ECOW_AllState = ECOW_Position
	};

	static	GameObject*	StaticCreate() { return new CowObject(); }

	virtual uint32_t	GetAllStateMask()	const override { return ECOW_AllState; }
	//virtual uint32_t	Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;
	//virtual void		Read(InputMemoryBitStream& inInputStream) override;

	

};

