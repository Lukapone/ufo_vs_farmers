#pragma once

enum class SoundID
{
	Beam,
	Gun,
	UFOHit,
	Cow
};