#pragma once
class GameObject;

class ObjectManagerV2
{
private:

	bool m_bPause{ false }; //for pausing the game
	bool m_bPauseHUD{ false };

	std::vector<std::shared_ptr<GameObject>> m_updateList;//for now only use update list

	//que the objects for add in next frame
	std::vector<std::shared_ptr<GameObject>> m_AddList;
	std::vector<int> m_disposeList;
	
	std::vector <std::vector<std::shared_ptr<GameObject>>> m_drawList;
	std::vector < std::shared_ptr<GameObject>> m_drawListZ0;
	std::vector < std::shared_ptr<GameObject>> m_drawListZ1;
	std::vector < std::shared_ptr<GameObject>> m_drawListZ2;

	std::vector <std::shared_ptr<GameObject>> m_HUDList;
	std::vector<std::shared_ptr<GameObject>> m_AddListHUD;
public:

	static std::unique_ptr< ObjectManagerV2 >		sInstance;

	static void StaticInit();


	void Pause();
	void PauseHUD();

	void AddtoHUD(std::shared_ptr<GameObject> toAdd);
	void AddNewObjectsHUD();
	void UpdateHUD(sf::Time sinceStart, sf::Time elapsed);
	int DrawHUD(sf::RenderWindow & renderWindow, sf::RectangleShape viewRectangle);
	int DrawDebugHUD(sf::RenderWindow & renderWindow);

	void Add(std::shared_ptr<GameObject> toAdd);
	void Remove(std::shared_ptr<GameObject> toRemove);
	void RemoveFromVector(int index);
	int GetObjectIndex(std::shared_ptr<GameObject> toGet) const;
    std::vector<std::shared_ptr<GameObject>> getUpdateList() { return m_updateList; }
	void DisposeRemoved();
	void AddNewObjects();

	int setDrawList(sf::RectangleShape viewRectangle);

	int Update();
	int Update(sf::Time sinceStart, sf::Time elapsed);
	int Draw(sf::RenderWindow & renderWindow, sf::RectangleShape viewRectangle);
	int Draw(sf::RenderWindow & renderWindow);
	int DrawDebug(sf::RenderWindow & renderWindow);


	// maybe used functions
	/*void DisposeUnused();
	int SetDrawList(Camera2D camera);
	void Dispose();*/
};