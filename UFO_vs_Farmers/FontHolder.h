#pragma once

class FontHolder
{
private:
	std::map<FontID, std::unique_ptr<sf::Font >> mFontMap;
public:
	static std::unique_ptr< FontHolder >	sInstance;


	static void StaticInit();
	void loadFonts();

	void load(FontID id, const std::string& filename);
	
	sf::Font& get(FontID id);
	const sf::Font& get(FontID id) const;

};