#include"UFO_FarmersShared.h"


//used in ufo for physics made by Lukas
void accelerate(ObjectState & state, float t)
{
	state.velocity = state.velocity + state.acceleration * t;
}

void accelerate_y(ObjectState & state, float t)
{
	state.velocity.y = state.velocity.y + state.acceleration.y * t;
}

void accelerate_x(ObjectState & state, float t)
{
	state.velocity.x = state.velocity.x + state.acceleration.x * t;
}

void deaccelerate(ObjectState & state, float t)
{
	state.velocity = state.velocity - state.acceleration * t * 2.0f;
}

void deaccelerate_y(ObjectState & state, float t)
{
	state.velocity.y = state.velocity.y - state.acceleration.y * t * 2.0f;
}

void deaccelerate_x(ObjectState & state, float t)
{
	state.velocity.x = state.velocity.x - state.acceleration.x * t * 2.0f;
}

void move(ObjectState & state, float t)
{
	state.position = state.position + state.velocity * t;
}
