#include"UFO_FarmersShared.h"

bool Move::Write( OutputMemoryBitStream& inOutputStream ) const
{
	
	mInputState.Write( inOutputStream );
	inOutputStream.Write( mTimestamp );
	//std::cout << "10.Write timestamp float 32bits  = " << inOutputStream.GetBitLength() << std::endl;
	//std::cout << "writting mTimestamp: " << mTimestamp << std::endl;
	return true;
}

bool Move::Read( InputMemoryBitStream& inInputStream )
{
	mInputState.Read( inInputStream );
	inInputStream.Read( mTimestamp );
	//std::cout << "reding mTimestamp: " << mTimestamp << std::endl;
	return true;
}
