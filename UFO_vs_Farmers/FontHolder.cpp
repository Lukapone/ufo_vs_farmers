#include"UFO_FarmersShared.h"

std::unique_ptr< FontHolder >	FontHolder::sInstance;

void FontHolder::StaticInit()
{
	sInstance = std::make_unique<FontHolder>();//http://stackoverflow.com/questions/16061407/should-i-assign-or-reset-a-unique-ptr
}

void FontHolder::loadFonts()
{
	//path to get to the folder with images
	std::string resourcePath = { "../Resources/" };
	FontHolder::sInstance->load(FontID::Arial, resourcePath + "arial.ttf");
}

void FontHolder::load(FontID id, const std::string & filename)
{
	std::unique_ptr<sf::Font> font(std::make_unique<sf::Font>());
	if (!font->loadFromFile(filename))
		throw std::runtime_error("TextureHolder::load - Failed to load " + filename);

	auto inserted = mFontMap.insert(std::make_pair(id, std::move(font)));
	assert(inserted.second);
}

sf::Font & FontHolder::get(FontID id)
{
	auto found = mFontMap.find(id);
	return *found->second;
}

const sf::Font & FontHolder::get(FontID id) const
{
	const auto found = mFontMap.find(id);
	return *found->second;
}
