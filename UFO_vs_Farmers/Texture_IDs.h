#pragma once

enum class TextureID
{
	UFO,
	Farmer,
	Cow,
	Beam,
	Background,
	Barn,
	Gun,
	Bullet,
	Cloud,
	Tree
};


enum class FontID
{
	Arial,
};




