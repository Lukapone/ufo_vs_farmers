#include"UFO_FarmersShared.h"

UFO_beam::UFO_beam()
{
	m_ZIndex = 1;
	sprite.setTexture(TextureHolder::sInstance->get(TextureID::Beam));
	
	collisionRect.setSize(sf::Vector2f(128, 254));
	collisionRect.setFillColor(sf::Color::Red);
	collisionRect.setScale(1, 1);
	collisionRect.setPosition(0, 0);
	collisionRect.setOrigin(64, 0);

	sprite.setOrigin(collisionRect.getOrigin());
	sprite.setPosition(collisionRect.getPosition());
	sprite.setScale(collisionRect.getScale());
	sprite.setRotation(collisionRect.getRotation());
}

void UFO_beam::Update(sf::Time sinceStart, sf::Time elapsed)
{
	sprite.setPosition(collisionRect.getPosition());
	sprite.setScale(collisionRect.getScale());
	sprite.setRotation(collisionRect.getRotation());

	checkAndApplyCollision(elapsed);
}

ClassIdentification UFO_beam::class_identification()
{
	return ClassIdentification::UFO_beam;
}

std::unique_ptr<GameObject> UFO_beam::Clone()
{
	return std::make_unique<UFO_beam>(*this);
}

std::shared_ptr<GameObject> UFO_beam::SharedClone()
{
	return std::make_shared<UFO_beam>(*this);
}

void UFO_beam::CenterBeamToUFO(UFO & ufo)
{
	collisionRect.setPosition(ufo.collisionRect.getPosition().x,ufo.collisionRect.getPosition().y+54);// +(ufo.collisionRect.getSize().y / 2 * ufo.collisionRect.getScale().y)) + 55
	//collisionRect.rotate(ufo.collisionRect.getRotation());
																									  /*collisionRect.setPosition(ufo.collisionRect.getPosition().x + 420, 
		ufo.collisionRect.getPosition().y + 250);*/
}


//if the beam intersect cow it will take periodical damage from cow and then destroy the cow made by Lukas
void UFO_beam::checkAndApplyCollision(sf::Time elapsed)
{
	for (auto & gameObj : GameObject::objectManager->getUpdateList())
	{
		if (gameObj->class_identification() == ClassIdentification::CowObject)
		{
			if (gameObj->collisionRect.getGlobalBounds().intersects(this->collisionRect.getGlobalBounds()))
			{
		
				m_timeSinceStartBeam += elapsed.asMilliseconds();
				if (m_timeSinceStartBeam > 200)//this is in milliseconds
				{
					std::cout << "damage " << std::endl;
					CowObject & objCow = dynamic_cast<CowObject&>(*gameObj);
					objCow.takeDamege(1);
					m_timeSinceStartBeam = 0;//reset the timer

					if (objCow.isDeath())
					{
						/*std::shared_ptr<GameObject>toAdd = gameObj->SharedClone();
						toAdd->collisionRect.setPosition(getRandomMt19937(100, 4000), 510);
						GameObject::objectManager->Add(toAdd);*/
						GameObject::objectManager->Remove(gameObj);
						//m_theCowCount->abductCow();
						return;
					}
				}
				//std::cout << "Beam intersecting cow" << std::endl;
				//GameObject::objectManager->Remove(gameObj);
				//break;
			}
		}
	}



}

void UFO_beam::setCowCount(std::shared_ptr<CowCounter> toSet)
{
	m_theCowCount = toSet;
}


