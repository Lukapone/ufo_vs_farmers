#include "UFO_Farmers_SharedHeaders.h"

std::unique_ptr< SoundHolder >	SoundHolder::sInstance;


void SoundHolder::StaticInit()
{
	sInstance = std::make_unique<SoundHolder>();
}

void SoundHolder::loadSoundBuffers()
{
	std::string resourcePath = { "../Resources/" };
	SoundHolder::sInstance->load(SoundID::Gun, resourcePath + "gunshot.wav");

}

void SoundHolder::load(SoundID id, const std::string & filename)
{
	std::unique_ptr<sf::SoundBuffer> soundBuffer(std::make_unique<sf::SoundBuffer>());
	if (!soundBuffer->loadFromFile(filename))
		throw std::runtime_error("SoundHolder::load - Failed to load " + filename);

	auto inserted = mSoundMap.insert(std::make_pair(id, std::move(soundBuffer)));
	assert(inserted.second);

	std::unique_ptr<sf::Sound> sounds(std::make_unique<sf::Sound>());
	sounds->setBuffer(*mSoundMap.find(id)->second);
	auto inserted2 = mPlaySoundsMap.insert(std::make_pair(id, std::move(sounds)));
	assert(inserted2.second);
}

void SoundHolder::play(SoundID id)
{
	auto found = mPlaySoundsMap.find(id);
	
	found->second->play();
	//system("PAUSE");
}
