#include"UFO_FarmersShared.h"

//made by Lukas

UFO::UFO() 
{
	
	text.setFont(FontHolder::sInstance->get(FontID::Arial));
	text.setCharacterSize(30);
	
	//text.setStyle(sf::Text::Bold);
	text.setColor(sf::Color::Red);


	sprite.setTexture(TextureHolder::sInstance->get(TextureID::UFO));
	sprite.setPosition(sf::Vector2f(300, 100));
	sprite.setOrigin(210, 125);
	sprite.setScale(0.5, 0.5);

	m_ZIndex = 2;
	//collisionRect.setSize(sf::Vector2f(420, 250));
	collisionRect.setSize(sf::Vector2f(400, 200));
	collisionRect.setFillColor(sf::Color::Red);
	collisionRect.setScale(0.5, 0.5);
	collisionRect.setOrigin(210,125);
	
	
	collisionRect.setPosition(300,30);//this is in centre of screen needs to be adapted to diferent resolutions
	m_objState.velocity = sf::Vector2f(0, 0);
	m_objState.position = sf::Vector2f(300, 100);
	

	/////////////////////////////CONTROLS///////////////////////////////
	m_maxVelocity = GB_UFO_MAX_VELOCITY;
	m_accelerationSides = GB_UFO_ACCELERATION_SIDES;
	m_accelerationUPDown = GB_UFO_ACCLERATION_UP_DOWN;

}

void UFO::Update()
{
	//base ufo update
	//std::cout << "base ufo update" << std::endl;
	/*collisionRect.setPosition(m_objState.position);
	sprite.setPosition(m_objState.position);
	sprite.setScale(collisionRect.getScale());
	sprite.setRotation(collisionRect.getRotation());*/

	//collisionRect.setPosition(m_objState.position);

	sprite.setPosition(m_objState.position);
	sprite.setRotation(collisionRect.getRotation());
	text.setPosition(m_objState.position.x - 50, m_objState.position.y - 100);
	collisionRect.setPosition(sprite.getPosition());
	//checkAndApplyCollision();
}

void UFO::Update(sf::Time sinceStart, sf::Time elapsed)
{
	collisionRect.setPosition(m_objState.position);
	sprite.setPosition(m_objState.position);
	sprite.setRotation(collisionRect.getRotation());
	text.setPosition(m_objState.position.x - 50,m_objState.position.y - 100);



	//HandleInput(sinceStart,elapsed);
	//checkAndApplyCollision();
	makeVulnerableAfterTime(elapsed);

}

ClassIdentification UFO::class_identification()
{
	return ClassIdentification::UFO;
}

std::unique_ptr<GameObject> UFO::Clone()
{
	return std::make_unique<UFO>(*this);
}

std::shared_ptr<GameObject> UFO::SharedClone()
{
	return std::make_shared<UFO>(*this);
}


//void UFO::HandleInput(sf::Time sinceStart, sf::Time elapsed)
//{
//	
//	
//	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) && !sf::Keyboard::isKeyPressed(sf::Keyboard::S))
//	{
//
//		if (!m_bAcceleratingUP)
//		{
//			m_bAcceleratingUP = true;
//			m_timePressed = sinceStart.asSeconds();
//			m_objState.acceleration.y = -m_accelerationUPDown;
//			m_velocityLock_y = false;
//		}
//
//		if (-m_objState.velocity.y < m_maxVelocity)
//		{
//			accelerate_y(m_objState, elapsed.asSeconds());
//		}
//	}
//	else
//	{
//		m_bAcceleratingUP = false;
//	}
//
//	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) && !sf::Keyboard::isKeyPressed(sf::Keyboard::W))
//	{
//		if (!m_bAcceleratingDown)
//		{
//			m_bAcceleratingDown = true;
//			m_timePressed = sinceStart.asSeconds();
//			m_objState.acceleration.y = m_accelerationUPDown;
//			m_velocityLock_y = false;
//		}
//
//		if (m_objState.velocity.y < m_maxVelocity)
//		{
//			accelerate_y(m_objState, elapsed.asSeconds());
//		}
//
//	}
//	else
//	{
//		m_bAcceleratingDown = false;
//	}
//
//	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) && !sf::Keyboard::isKeyPressed(sf::Keyboard::D))
//	{
//
//		if (!m_bAcceleratingLeft)
//		{
//			m_bAcceleratingLeft = true;
//			m_timePressed = sinceStart.asSeconds();
//			m_objState.acceleration.x = -m_accelerationSides;
//			m_bRotatingLeft = true;
//			m_velocityLock_x = false;
//		}
//
//		if (-m_objState.velocity.x < m_maxVelocity)
//		{
//			accelerate_x(m_objState, elapsed.asSeconds());
//
//		}
//
//		if (m_rotatedRightAngle > 0)
//		{
//			m_rotatedLeftAngle -= m_rotatedRightAngle;
//			m_rotatedRightAngle = 0;
//		}
//		if (m_rotatedLeftAngle < 45)
//		{
//			m_rotatedLeftAngle += 0.5;
//			collisionRect.rotate(-0.5);
//		}
//
//	}
//	else
//	{
//		m_bRotatingLeft = false;
//		m_bAcceleratingLeft = false;
//
//	}
//
//	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D) && !sf::Keyboard::isKeyPressed(sf::Keyboard::A))
//	{
//
//		if (!m_bAcceleratingRight)
//		{
//			m_bAcceleratingRight = true;
//			m_timePressed = sinceStart.asSeconds();
//			m_objState.acceleration.x = m_accelerationSides;
//			m_velocityLock_x = false;
//			m_bRotatingRight = true;
//		}
//
//		if (m_objState.velocity.x < m_maxVelocity)
//		{
//			accelerate_x(m_objState, elapsed.asSeconds());
//		}
//		
//		if (m_rotatedLeftAngle > 0)
//		{
//			m_rotatedRightAngle -= m_rotatedLeftAngle;
//			m_rotatedLeftAngle = 0;
//		}
//		if (m_rotatedRightAngle < 45)
//		{
//			m_rotatedRightAngle += 0.5;
//			collisionRect.rotate(0.5);
//		}
//	}
//	else
//	{
//
//		m_bRotatingRight = false;
//		m_bAcceleratingRight = false;
//
//		
//		
//	}
//
//	//if you are not pressing left or right you will need to check the velocity if is positive or negative and acceleration and if they match proceed
//	if (!m_bAcceleratingLeft && !m_bAcceleratingRight)
//	{
//		if (!m_velocityLock_x)
//		{
//			m_velocityLock_x = true;
//			m_is_positive_x = m_objState.velocity.x > 0;
//
//		}
//		if (m_is_positive_x && m_objState.velocity.x > 0)
//		{
//			if (m_objState.acceleration.x > 0)
//			{
//				deaccelerate_x(m_objState, elapsed.asSeconds());
//			}
//			else
//			{
//				accelerate_x(m_objState, elapsed.asSeconds());
//			}
//		}
//		else if (!m_is_positive_x && -m_objState.velocity.x > 0)
//		{
//			if (-m_objState.acceleration.x > 0)
//			{
//				deaccelerate_x(m_objState, elapsed.asSeconds());
//			}
//			else
//			{
//				accelerate_x(m_objState, elapsed.asSeconds());
//			}
//		}
//		else
//		{
//			m_objState.velocity.x = 0;
//		}
//	}
//
//	if (!m_bAcceleratingDown && !m_bAcceleratingUP)
//	{
//		if (!m_velocityLock_y)
//		{
//			m_velocityLock_y = true;
//			m_is_positive_y = m_objState.velocity.y > 0;
//
//		}
//			
//		if (m_is_positive_y && m_objState.velocity.y > 0)
//		{
//			if (m_objState.acceleration.y > 0)
//			{
//				deaccelerate_y(m_objState, elapsed.asSeconds());
//			}
//			else
//			{
//				accelerate_y(m_objState, elapsed.asSeconds());
//			}
//			
//		}
//		else if (!m_is_positive_y && -m_objState.velocity.y > 0)
//		{
//			if (-m_objState.acceleration.y > 0)
//			{
//				deaccelerate_y(m_objState, elapsed.asSeconds());
//			}
//			else
//			{
//				accelerate_y(m_objState, elapsed.asSeconds());
//			}
//		}
//		else
//		{
//			m_objState.velocity.y = 0;
//		}
//	}
//
//	////////////////////////////////////////// APPLY MOVEMENT //////////////////////////////////////////////////////
//	move(m_objState, elapsed.asSeconds());
//
//
//
//	if (!m_bRotatingLeft && !m_bRotatingRight)
//	{
//		if (m_rotatedLeftAngle > 0)
//		{
//			m_rotatedLeftAngle -= 0.5;
//			collisionRect.rotate(0.5);
//		}
//		else if (m_rotatedRightAngle > 0)
//		{
//			m_rotatedRightAngle -= 0.5;
//			collisionRect.rotate(-0.5);
//		}
//		else
//		{
//			collisionRect.setRotation(0);
//		}
//
//
//	}
//
//	//if (sf::Keyboard::isKeyPressed(sf::Keyboard::X))
//	//{
//	//	////collisionRect.setOrigin(0, 0);
//	//	//collisionRect.setOrigin(210, 125);
//	//	//sprite.setOrigin(collisionRect.getOrigin());
//	//	//collisionRect.rotate(1);
//	//	///*collisionRect.setOrigin(0, 0);
//	//	//sprite.setOrigin(collisionRect.getOrigin());*/
//	//	takeDamage(1);
//	//}
//
//	if (sf::Keyboard::isKeyPressed(sf::Keyboard::C))
//	{
//		
//		if (m_objState.velocity.x == 0 && m_objState.velocity.y == 0)
//		{
//			m_bBeaming = true;
//		}
//		else
//		{
//			m_bBeaming = false;
//			if (m_bBeamActive)
//			{
//				GameObject::objectManager->Remove(m_UFO_beam);
//				m_bBeamActive = false;
//			}
//		}
//	}
//	else
//	{
//		m_bBeaming = false;
//		if (m_bBeamActive)
//		{
//			GameObject::objectManager->Remove(m_UFO_beam);
//			m_bBeamActive = false;
//		}
//	
//	}
//
//	m_previousBeamKeyState = sf::Keyboard::isKeyPressed(sf::Keyboard::C);
//
//	if (m_bBeaming)
//	{
//		//AddAndUpdateBeam();
//	}
//}

void UFO::checkAndApplyCollision(sf::Vector2f oldPos)
{
	if (m_objState.position.x < 0 && m_objState.velocity.x < 0)
	{
		m_objState.velocity.x = 0;
		m_objState.acceleration.x = 0;
		//this needs to be set as after input was processed collison is after the input
		m_objState.position.x = oldPos.x;
		sprite.setPosition(oldPos);

		//std::cout << "below zero x " << std::endl;
	}

	if (m_objState.position.x > 4068 && m_objState.velocity.x > 0)
	{
		m_objState.velocity.x = 0;
		m_objState.acceleration.x = 0;
		m_objState.position.x = oldPos.x;
		sprite.setPosition(oldPos);
		//std::cout << "abowe map border" << std::endl;
	}

	if(m_objState.position.y < 0 && m_objState.velocity.y < 0)
	{
		m_objState.velocity.y = 0;
		m_objState.acceleration.y = 0;
		m_objState.position.y = oldPos.y;
		sprite.setPosition(oldPos);
		//std::cout << "below zero y " << std::endl;
	}

	if (m_objState.position.y > 250 && m_objState.velocity.y > 0)
	{
		m_objState.velocity.y = 0;
		m_objState.acceleration.y = 0;
		m_objState.position.y = oldPos.y;
		sprite.setPosition(oldPos);
		//std::cout << "below mid y " << std::endl;
	}
}

void UFO::takeDamage(int32_t amount)
{
	if (m_canBeHit == true)
	{
		m_canBeHit = false;
		m_health -= amount;
		text.setString(m_PlayerName + ": " + std::to_string(m_health));
		m_timeSinceLastHit = 0;
	}

}

void UFO::makeVulnerableAfterTime(sf::Time elapsed)
{

	m_timeSinceLastHit += elapsed.asMilliseconds();
	//std::cout << "since last hit: " << m_timeSinceLastHit << std::endl;
	if (m_timeSinceLastHit > 1000)//this is in milliseconds
	{
		m_canBeHit = true;
	}
}

bool UFO::isDeath() const
{
	if (m_health <= 0)
	{
		return true;
	}

	return false;
}

uint32_t UFO::Write(OutputMemoryBitStream & inOutputStream, uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	if (inDirtyState & ECRS_PlayerId)
	{
		inOutputStream.Write((bool)true);
		inOutputStream.Write(GetPlayerId(),8);
		inOutputStream.Write(m_PlayerName);
		writtenState |= ECRS_PlayerId;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}


	if (inDirtyState & ECRS_PositionChange)
	{
		inOutputStream.Write((bool)true);
		uint32_t posX = sprite.getPosition().x;
		uint32_t posY = sprite.getPosition().y;
		inOutputStream.Write(posX,12);
		inOutputStream.Write(posY,12);

		writtenState |= ECRS_PositionChange;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}


	if (inDirtyState & ECRS_Health)
	{
		inOutputStream.Write((bool)true);
		inOutputStream.Write(m_health, 5);

		writtenState |= ECRS_Health;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	if (inDirtyState & ECRS_Rotation)
	{
		inOutputStream.Write((bool)true);
		uint32_t rotationInt = sprite.getRotation();
		inOutputStream.Write(rotationInt,9);

		writtenState |= ECRS_Rotation;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	//std::cout << "after using WRITE the writtenstate is : " << writtenState << std::endl;
	return writtenState;
}


//void UFO::ProcessInput(float elapsed, const InputState & inInputState)
//{
//	//std::cout << "Processing input from input state: " << std::endl;
//	//std::cout << "is W pressed: " << inInputState.is_W_pressed() << std::endl;
//	//std::cout << "is S pressed: " << inInputState.is_S_pressed() << std::endl;
//	//std::cout << "is A pressed: " << inInputState.is_A_pressed() << std::endl;
//	//std::cout << "is D pressed: " << inInputState.is_D_pressed() << std::endl;
//	//std::cout << "is C pressed: " << inInputState.is_C_pressed() << std::endl;
//	//std::cout << "Delta time for input is: " << elapsed << std::endl;
//
//
//	////process our input....
//	//std::cout << "processing input" << inInputState.GetDesiredHorizontalDelta() << " and " << inInputState.GetDesiredVerticalDelta() << std::endl;
//	//sprite.move(inInputState.GetDesiredHorizontalDelta(), inInputState.GetDesiredVerticalDelta());
//	//m_bBeaming = inInputState.IsShooting();//print here if working
//
//	//if (m_bBeaming)
//	//{
//	//	std::cout << "beam active" << std::endl;
//	//	//AddAndUpdateBeam();
//	//}
//
//	/*if (inInputState.is_C_pressed())
//	{
//		m_bBeaming = true;
//	}*/
//
//	if (inInputState.is_W_pressed() && !inInputState.is_S_pressed())
//	{
//
//		if (!m_bAcceleratingUP)
//		{
//			m_bAcceleratingUP = true;
//			m_objState.acceleration.y = -m_accelerationUPDown;
//			m_velocityLock_y = false;
//		}
//
//		if (-m_objState.velocity.y < m_maxVelocity)
//		{
//			accelerate_y(m_objState, elapsed);
//		}
//	}
//	else
//	{
//		m_bAcceleratingUP = false;
//	}
//
//	if (inInputState.is_S_pressed() && !inInputState.is_W_pressed())
//	{
//		if (!m_bAcceleratingDown)
//		{
//			m_bAcceleratingDown = true;
//			m_objState.acceleration.y = m_accelerationUPDown;
//			m_velocityLock_y = false;
//		}
//
//		if (m_objState.velocity.y < m_maxVelocity)
//		{
//			accelerate_y(m_objState, elapsed);
//		}
//
//	}
//	else
//	{
//		m_bAcceleratingDown = false;
//	}
//
//	if (inInputState.is_A_pressed() && !inInputState.is_D_pressed())
//	{
//
//		if (!m_bAcceleratingLeft)
//		{
//			m_bAcceleratingLeft = true;
//			m_objState.acceleration.x = -m_accelerationSides;
//			m_bRotatingLeft = true;
//			m_velocityLock_x = false;
//		}
//
//		if (-m_objState.velocity.x < m_maxVelocity)
//		{
//			accelerate_x(m_objState, elapsed);
//
//		}
//
//		if (m_rotatedRightAngle > 0)
//		{
//			m_rotatedLeftAngle -= m_rotatedRightAngle;
//			m_rotatedRightAngle = 0;
//		}
//		if (m_rotatedLeftAngle < 45)
//		{
//			m_rotatedLeftAngle += 0.5;
//			collisionRect.rotate(-0.5);
//		}
//
//	}
//	else
//	{
//		m_bRotatingLeft = false;
//		m_bAcceleratingLeft = false;
//
//	}
//
//	if (inInputState.is_D_pressed() && !inInputState.is_A_pressed())
//	{
//
//		if (!m_bAcceleratingRight)
//		{
//			m_bAcceleratingRight = true;
//			m_objState.acceleration.x = m_accelerationSides;
//			m_velocityLock_x = false;
//			m_bRotatingRight = true;
//		}
//
//		if (m_objState.velocity.x < m_maxVelocity)
//		{
//			accelerate_x(m_objState, elapsed);
//		}
//
//		if (m_rotatedLeftAngle > 0)
//		{
//			m_rotatedRightAngle -= m_rotatedLeftAngle;
//			m_rotatedLeftAngle = 0;
//		}
//		if (m_rotatedRightAngle < 45)
//		{
//			m_rotatedRightAngle += 0.5;
//			collisionRect.rotate(0.5);
//		}
//	}
//	else
//	{
//
//		m_bRotatingRight = false;
//		m_bAcceleratingRight = false;
//
//
//
//	}
//
//	//if you are not pressing left or right you will need to check the velocity if is positive or negative and acceleration and if they match proceed
//	if (!m_bAcceleratingLeft && !m_bAcceleratingRight)
//	{
//		if (!m_velocityLock_x)
//		{
//			m_velocityLock_x = true;
//			m_is_positive_x = m_objState.velocity.x > 0;
//
//		}
//		if (m_is_positive_x && m_objState.velocity.x > 0)
//		{
//			if (m_objState.acceleration.x > 0)
//			{
//				deaccelerate_x(m_objState, elapsed);
//			}
//			else
//			{
//				accelerate_x(m_objState, elapsed);
//			}
//		}
//		else if (!m_is_positive_x && -m_objState.velocity.x > 0)
//		{
//			if (-m_objState.acceleration.x > 0)
//			{
//				deaccelerate_x(m_objState, elapsed);
//			}
//			else
//			{
//				accelerate_x(m_objState, elapsed);
//			}
//		}
//		else
//		{
//			m_objState.velocity.x = 0;
//		}
//	}
//
//	if (!m_bAcceleratingDown && !m_bAcceleratingUP)
//	{
//		if (!m_velocityLock_y)
//		{
//			m_velocityLock_y = true;
//			m_is_positive_y = m_objState.velocity.y > 0;
//
//		}
//
//		if (m_is_positive_y && m_objState.velocity.y > 0)
//		{
//			if (m_objState.acceleration.y > 0)
//			{
//				deaccelerate_y(m_objState, elapsed);
//			}
//			else
//			{
//				accelerate_y(m_objState, elapsed);
//			}
//
//		}
//		else if (!m_is_positive_y && -m_objState.velocity.y > 0)
//		{
//			if (-m_objState.acceleration.y > 0)
//			{
//				deaccelerate_y(m_objState, elapsed);
//			}
//			else
//			{
//				accelerate_y(m_objState, elapsed);
//			}
//		}
//		else
//		{
//			m_objState.velocity.y = 0;
//		}
//	}
//
//	////////////////////////////////////////// APPLY MOVEMENT //////////////////////////////////////////////////////
//	move(m_objState, elapsed);
//
//
//
//	if (!m_bRotatingLeft && !m_bRotatingRight)
//	{
//		if (m_rotatedLeftAngle > 0)
//		{
//			m_rotatedLeftAngle -= 0.5;
//			collisionRect.rotate(0.5);
//		}
//		else if (m_rotatedRightAngle > 0)
//		{
//			m_rotatedRightAngle -= 0.5;
//			collisionRect.rotate(-0.5);
//		}
//		else
//		{
//			collisionRect.setRotation(0);
//		}
//
//
//	}
//
//	if (inInputState.is_C_pressed())
//	{
//
//		if (m_objState.velocity.x == 0 && m_objState.velocity.y == 0)
//		{
//			m_bBeaming = true;
//		}
//		else
//		{
//			m_bBeaming = false;
//			if (m_bBeamActive)
//			{
//				GameObject::objectManager->Remove(m_UFO_beam);
//				m_bBeamActive = false;
//			}
//		}
//	}
//	else
//	{
//		m_bBeaming = false;
//		if (m_bBeamActive)
//		{
//			GameObject::objectManager->Remove(m_UFO_beam);
//			m_bBeamActive = false;
//		}
//
//	}
//
//	m_previousBeamKeyState = inInputState.is_C_pressed();
//
//	if (m_bBeaming)
//	{
//		AddAndUpdateBeam();
//	}
//
//}

void UFO::SimulateMovement(float inDeltaTime)
{
	//simulate us...
	
}

