#pragma once
class BulletObject;
#include "GlobalVariables.h"

class GunObject : public GameObject
{

	private:
	protected:
		//int m_rotation = { 0 };
		int m_timeSinceLastRotate = { GB_GUN_TIME_SINCE_LAST_ROTATE };
		bool m_shotLock{ GB_GUN_SHOT_LOCK };
		int m_rotationInDegrees{ GB_GUN_ROTATION_IN_DEGREES };
		int m_timeSinceLastShot{ GB_GUN_TIME_SINCE_LAST_SHOT };

	public:
		std::shared_ptr<BulletObject> m_Bullet;
		GunObject();
		CLASS_IDENTIFICATION('CGUN', GameObject)

		enum EGUNReplicationState
		{
			EGUN_PositionChange = 1 << 0,
			EGUN_Rotation = 1 << 1,

			EGUN_AllState = EGUN_PositionChange | EGUN_Rotation
		};

		static	GameObject*	StaticCreate() { return new GunObject(); }

		virtual uint32_t GetAllStateMask()	const override { return EGUN_AllState; }

		virtual uint32_t Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;

		//OVERRIDEN VIRTUAL FUNCTIONS
		virtual void Update(sf::Time sinceStart, sf::Time elapsed) override;
		virtual void Update() override;
		virtual ClassIdentification class_identification() override;
		virtual std::unique_ptr<GameObject> Clone() override;
		virtual std::shared_ptr<GameObject> SharedClone() override;

		//class functions
		void HandleInput(sf::Time sinceStart, sf::Time elapsed);
		void Shoot(sf::Time sinceStart);
		inline void SetBullet(std::shared_ptr<BulletObject> & toSet) { m_Bullet = toSet; }
};