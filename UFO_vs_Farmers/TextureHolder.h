#pragma once

class TextureHolder
{
private:
	std::map<TextureID , std::unique_ptr<sf::Texture >> mTextureMap;
public:
	static std::unique_ptr< TextureHolder >	sInstance;


	static void StaticInit();
	void loadTextures();

	void load(TextureID id, const std::string& filename);
	void load(TextureID id, const std::string& filename, sf::IntRect inRect);
	sf::Texture& get(TextureID id);
	const sf::Texture& get(TextureID id) const;

};

