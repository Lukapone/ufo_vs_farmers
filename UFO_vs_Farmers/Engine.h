

class Engine
{
public:
	sf::RenderWindow mWindow;
	virtual ~Engine();
	static std::unique_ptr< Engine >	sInstance;

	virtual int		Run();
	void			SetShouldKeepRunning(bool inShouldKeepRunning) { mShouldKeepRunning = inShouldKeepRunning; }
	virtual void processEvents();
	
	void render();

	Engine();
protected:
	
	virtual void	DoFrame();
	
private:

	

	int		DoRunLoop();

	bool	mShouldKeepRunning;



};