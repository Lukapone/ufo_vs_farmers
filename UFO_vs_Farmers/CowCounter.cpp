#include"UFO_FarmersShared.h"

CowCounter::CowCounter()
{
	m_ZIndex = 2;
	text.setFont(FontHolder::sInstance->get(FontID::Arial));
	text.setCharacterSize(30);
	text.setColor(sf::Color::Red);
	text.setPosition(10,65);
	//text.setPosition(0, 0);
}


void CowCounter::Update(sf::Time sinceStart, sf::Time elapsed)
{

}


void CowCounter::Update()
{
	text.setString("Cow count: " + std::to_string(m_cowCount));
}

ClassIdentification CowCounter::class_identification()
{
	return ClassIdentification::GameObject;
}

std::unique_ptr<GameObject> CowCounter::Clone()
{
	return std::make_unique<CowCounter>(*this);
}

std::shared_ptr<GameObject> CowCounter::SharedClone()
{
	return std::make_shared<CowCounter>(*this);
}

void CowCounter::setCount(int32_t toSet)
{
	m_cowCount = toSet;
}

int32_t CowCounter::getCount() const
{
	return m_cowCount;
}

void CowCounter::setVictoryScreen(std::shared_ptr<GameObject> victoryScreen)
{
	m_victoryScreen = victoryScreen;
}

void CowCounter::abductCow()
{
	m_cowCount--;
	text.setString("Cow counter: " + std::to_string(m_cowCount));
	/*if (m_cowCount == 0)
	{
		GameObject::objectManager->AddtoHUD(m_victoryScreen);
		GameObject::objectManager->Pause();
	}*/
}


