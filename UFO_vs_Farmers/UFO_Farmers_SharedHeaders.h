#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX

#include "Windows.h"
#include "WinSock2.h"
#include "Ws2tcpip.h"
typedef int socklen_t;
//typedef char* receiveBufer_t;
#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <netdb.h>
#include <errno.h>
#include <fcntl.h>
//typedef void* receiveBufer_t;
typedef int SOCKET;
const int NO_ERROR = 0;
const int INVALID_SOCKET = -1;
const int WSAECONNRESET = ECONNRESET;
const int WSAEWOULDBLOCK = EAGAIN;
const int SOCKET_ERROR = -1;
#endif

//standard includes always first !!!!!!!!!!!!!!!!!
#include<deque>
#include<queue>
#include<unordered_set>
#include<unordered_map>
#include<list>
#include<cassert>
#include<map>

#include<iostream>
#include<string>
#include<vector>
#include<memory>
#include<random>
#include<cmath>
#include<string>


//sfml includes
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

//custom class includes
#include"GameObject.h"
#include"LoadResources.h"
#include"ObjectManagerV2.h"
#include"UFO.h"
#include"UFO_beam.h"
#include"CowObject.h"
#include"FarmerObject.h"
#include"GunObject.h"
#include"CowCounter.h"
#include "BulletObject.h"
#include "Background.h"

#include"EnumForIdentification.h"
#include "Math_random_helper_functions.h"
#include "PhysicsIntegration.h"
#include "Texture_IDs.h"
#include "ResourceHolder.h"
#include "TextureHolder.h"
#include "FontHolder.h"
#include "Sound_IDs.h"
#include "SoundHolder.h"

#include "GlobalVariables.h"

//includes from robocat

//class RoboCat;
class GameObject;


#include "RoboMath.h"

#include "StringUtils.h"
#include "SocketAddress.h"
#include "SocketAddressFactory.h"
#include "UDPSocket.h"
#include "TCPSocket.h"
#include "SocketUtil.h"

//#include "ByteSwap.h"
//#include "LinkingContext.h"
#include "MemoryBitStream.h"

#include "TransmissionData.h"
#include "InFlightPacket.h"
#include "AckRange.h"
#include "DeliveryNotificationManager.h"

#include "InputAction.h"
#include "InputState.h"
#include "Move.h"
#include "MoveList.h"

//#include "GameObject.h"
#include "GameObjectRegistry.h"
//#include "RoboCat.h"
//#include "World.h"
#include "Timing.h"
//#include "Mouse.h"
//#include "Yarn.h"
#include "StringUtils.h"
//#include "ScoreBoardManager.h"

#include "WeightedTimedMovingAverage.h"
#include "ReplicationCommand.h"
#include "NetworkManager.h"
#include "Engine.h"

